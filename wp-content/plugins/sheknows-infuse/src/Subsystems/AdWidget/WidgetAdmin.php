<?php

namespace SheKnows\Infuse\Subsystems\AdWidget;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Core\AdTypes;
use SheKnows\Infuse\Core\Options;
use SheKnows\Infuse\Core\ViewFactory;

/**
 * Class WidgetAdmin
 *
 * @package SheKnows\Infuse\Subsystems\AdWidget
 */
class WidgetAdmin {

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var AdTypes
	 */
	protected $adTypes;

	/**
	 * @var ViewFactory
	 */
	protected $viewFactory;

	/**
	 * WidgetAdmin constructor.
	 *
	 * @param Plugin      $plugin
	 * @param Options     $options
	 * @param AdTypes     $adTypes
	 * @param ViewFactory $viewFactory
	 */
	public function __construct( Plugin $plugin, Options $options, AdTypes $adTypes, ViewFactory $viewFactory ) {
		$this->plugin      = $plugin;
		$this->options     = $options;
		$this->adTypes     = $adTypes;
		$this->viewFactory = $viewFactory;
	}

	/**
	 * Gets an instance of the widgetadmin view and adds to the widget on the
	 * admin screen
	 *
	 * @param  Widget  $widget
	 * @param  array   $instance
	 * @return string
	 */
	public function renderForm( $widget, $instance ) {
		if ( isset( $instance['adtype'] ) && $this->adTypes->getTypeByName( $instance['adtype'] ) ) {
			$currentType = $instance['adtype'];
		} else {
			$adTypes     = array_keys( $this->adTypes->getAllTypes() );
			$currentType = array_shift( $adTypes );
		}

		if ( isset( $instance['advertisement_callout'] ) ) {
			$adCallout = $instance['advertisement_callout'];
		} else {
			$adCallout = null;
		}

		$args = array(
			'adTypeFieldId'   => $widget->get_field_id( 'adtype' ),
			'adTypeFieldName' => $widget->get_field_name( 'adtype' ),
			'adTypeOptions'   => $this->buildAdTypeOptions(),
			'currentType'     => $currentType,
			'adCalloutId'     => $widget->get_field_id( 'advertisement_callout' ),
			'adCalloutName'   => $widget->get_field_name( 'advertisement_callout' ),
			'adCallout'       => $adCallout,
		);

		return $this->viewFactory->make( 'admin/adwidget', $args )->render();
	}

	/**
	 * Validate form submissions from widget admin.
	 *
	 * @param  array   $instance
	 * @return array|false
	 */
	public function update( $instance ) {
		$validTypes = array_keys( $this->adTypes->getAllTypes() );

		if ( ! in_array( $instance['adtype'], $validTypes, true ) ) {
			return false;
		}

		return $instance;
	}

	/**
	 * Build the array of available ad types for the widget types dropdown.
	 *
	 * @return array  Array for building the <option> markup.
	 */
	protected function buildAdTypeOptions() {
		$options = array();

		foreach ( $this->adTypes->getAllTypes() as $type => $info ) {
			$newOption = array(
				'value' => $type,
				'text'  => $info['friendlyName'],
			);

			if ( isset( $info['sizes'] ) ) {
				$newOption['text'] .= ' (' . implode( ', ', $info['sizes'] ) . ')';
			}

			$options[] = $newOption;
		}

		return $options;
	}
}
