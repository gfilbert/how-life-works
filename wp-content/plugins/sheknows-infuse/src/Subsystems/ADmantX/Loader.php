<?php

namespace SheKnows\Infuse\Subsystems\ADmantX;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Loads required classes for making server to server requests to ADmantX and
 * caching the results.
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\ADmantX
 */
class Loader extends SubsystemLoader {

	/**
	 * Initializes binding relevant classes and relevant WordPress API hooks.
	 * Note that while bound classes may not load if not required
	 */
	public function load() {
		$this->bindSubsystemClasses();
		$this->registerSubsystemHooks();
	}

	/**
	 * Makes relevant classes available at runtime. If class has not been
	 * instantiated, the class is instantiated (with dependencies added through
	 * the constructor) and handed over. If the class has been instantiated,
	 * the class is handed over.
	 */
	protected function bindSubsystemClasses() {
		$this->container->bind( __NAMESPACE__ . '\Admants', array( $this, 'bindAdmants' ) );
		$this->container->bind( __NAMESPACE__ . '\Fetch', array( $this, 'bindFetch' ) );
	}

	/**
	 * Adds hooks to the WordPress API. Delays serving of ads.txt until after plugins are loaded.
	 */
	public function registerSubsystemHooks() {
		add_filter( 'sheknows_infuse_global_targeting', array( $this, 'maybeAddAdmantsTargeting' ), 10, 2 );
	}

	/**
	 * Constructs Admants class.
	 *
	 * @return Admants
	 */
	public function bindAdmants() {
		$dataCacheFactory = $this->container->resolve( 'SheKnows\Infuse\Core\DataCacheFactory' );
		$siteConfig       = $this->container->resolve( 'SheKnows\Infuse\Core\UpstreamSiteConfig' );
		$commonConfig     = $this->container->resolve( 'SheKnows\Infuse\Core\UpstreamCommonConfig' );
		$fetch            = $this->container->resolve( __NAMESPACE__ . '\Fetch' );

		return new Admants( $dataCacheFactory, $fetch, $siteConfig, $commonConfig );
	}

	/**
	 * Constructs Fetch class.
	 *
	 * @return Fetch
	 */
	public function bindFetch() {
		return new Fetch();
	}

	/**
	 * If enabled, grab and inject the admants into the global targeting.
	 *
	 * @param array $targeting
	 * @param array $vars
	 */
	public function maybeAddAdmantsTargeting( $targeting, $vars ) {

		$enabled = $this->container
					->resolve( 'SheKnows\Infuse\Core\UpstreamServiceStatus' )
					->isEnabled( 'admantx_infuse' );

		if ( $enabled ) {
			$targeting['admants'] = $this->container->resolve( __NAMESPACE__ . '\Admants' )->getAdmants( $vars );
		}

		return $targeting;
	}

}
