<?php

namespace SheKnows\Infuse\Subsystems\PluginsDeactivator;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Loads required classes for Competing Plugins Deactivation.
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\PluginsDeactivator
 */
class Loader extends SubsystemLoader {
	/**
	 * The slug for our competing plugins page.
	 *
	 * @var string
	 */
	protected $slug;

	/**
	 * The full hook name for our competing plugins page.
	 *
	 * @var string
	 */
	protected $hookName;

	/**
	 * Includes needed libs and performs initialization of needed classes.
	 */
	public function load() {
		$plugin         = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$this->slug     = $plugin->getSlug() . '-competing-plugins';
		$this->hookName = 'admin_page_' . $this->slug;

		$this->bindSubsystemClasses();
		$this->registerSubsystemHooks();
	}

	/**
	 * Makes helper classes available.
	 */
	protected function bindSubsystemClasses() {
		$this->container->bind( __NAMESPACE__ . '\Deactivator', array( $this, 'bindDeactivator' ) );
		$this->container->bind( __NAMESPACE__ . '\Tracker', array( $this, 'bindTracker' ), 'SubsystemTracker' );
	}

	/**
	 * Adds hooks for initialization.
	 */
	protected function registerSubsystemHooks() {
		if ( is_admin() ) {
			$deactivator         = $this->container->resolve( __NAMESPACE__ . '\Deactivator' );
			$hasCompetingPlugins = $deactivator->checkPlugins();

			if ( $hasCompetingPlugins ) {
				add_action( 'plugins_loaded', array( $this, 'initListPluginsPage' ) );
				add_action( 'admin_enqueue_scripts', array( $this, 'enqueueAdminAssets' ) );
				add_action( 'admin_notices', array( $this, 'displayNag' ) );
			}
		}
	}

	/**
	 * Constructs Deactivator class.
	 *
	 * @return Deactivator
	 */
	public function bindDeactivator() {
		$viewFactory   = $this->container->resolve( 'SheKnows\Infuse\Core\ViewFactory' );
		$commonConfig  = $this->container->resolve( 'SheKnows\Infuse\Core\UpstreamCommonConfig' );
		$siteConfig    = $this->container->resolve( 'SheKnows\Infuse\Core\UpstreamSiteConfig' );
		$errorLogging  = $this->container->resolve( 'SheKnows\Infuse\Core\ErrorLogging' );
		$pluginsHelper = $this->container->resolve( 'SheKnows\Infuse\Core\PluginsHelper' );
		$plugin        = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Deactivator( $viewFactory, $commonConfig, $siteConfig, $errorLogging, $pluginsHelper, $plugin, $this->slug );
	}

	/**
	 * Constructs Tracker class
	 *
	 * @return Tracker
	 */
	public function bindTracker() {
		$options     = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin      = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$deactivator = $this->container->resolve( __NAMESPACE__ . '\Deactivator' );

		return new Tracker( $options, $plugin, $deactivator );
	}

	/**
	 * Creates page with listed active competing plugins.
	 */
	public function initListPluginsPage() {
		add_submenu_page(
			'admin.php',
			__( 'Competing Plugins', 'sheknows-infuse' ),
			__( 'Competing Plugins', 'sheknows-infuse' ),
			'manage_options',
			$this->slug,
			array( $this, 'displayPluginsListPage' )
		);
	}

	/**
	 * Calls method to render the base with listed active competing plugins.
	 */
	public function displayPluginsListPage() {
		$deactivator = $this->container->resolve( __NAMESPACE__ . '\Deactivator' );
		$deactivator->displayPluginsList();
	}

	/**
	 * Enqueue the admin styles for the competing plugins page.
	 *
	 * @param string $hook The hook name of the current admin page.
	 */
	public function enqueueAdminAssets( $hook ) {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		if ( $hook === $this->hookName ) {
			wp_enqueue_style( $plugin->getSlug() . '-admin', plugin_dir_url( $plugin->getFile() ) . 'public/css/admin.css', array(), $plugin->getVersion() );
		}
	}

	/**
	 * Displays the nag to notify user about active competing plugins.
	 */
	public function displayNag() {
		// Don't display our nags on our own settings screen or on admin pages
		// where get_current_screen() doesn't exist.
		if ( ! function_exists( 'get_current_screen' ) || get_current_screen()->id === $this->hookName ) {
			return;
		}

		$class = 'notice notice-error is-dismissible';
		$url   = add_query_arg( array( 'page' => $this->slug ), admin_url( 'admin.php' ) );
		$link  = sprintf( '<a href="%s">click here</a>', $url );

		// translators: Prompt to disable competing plugins.
		$message = esc_html__( 'Potential SHE Media contract violations detected.  Please %s for details.', 'sheknows-infuse' );

		// phpcs:ignore WordPress.Security.EscapeOutput
		printf( '<div class="%s"><p><span class="dashicons dashicons-warning" style="color: #dc3232;"></span>&nbsp;%s</p></div>', esc_attr( $class ), sprintf( $message, $link ) );
	}
}
