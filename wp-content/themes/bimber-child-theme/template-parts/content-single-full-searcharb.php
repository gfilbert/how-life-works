<?php
/**
 * The default template for displaying single post content (with sidebar).
 * This is a template part. It must be used within The Loop.
 *
 * @package Bimber_Theme 5.4
 */

$bimber_entry_data = bimber_get_template_part_data();
$bimber_elements   = $bimber_entry_data['elements'];
?>
<?php do_action( 'bimber_before_single_content' ); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry-tpl-classic-nosidebar' ); ?> itemscope="" itemtype="<?php echo esc_attr( bimber_get_entry_microdata_itemtype() ); ?>">
	<div <?php bimber_render_entry_inner_class(); ?>>
		<?php get_template_part( 'template-parts/post/sponsor-top' ); ?>

		<header class="entry-header entry-header-01">
			<?php bimber_render_entry_title( '<h1 class="g1-giga g1-giga-1st entry-title" itemprop="headline">', '</h1>' ); ?>
		</header>

		<?php
		if ( bimber_show_entry_featured_media( $bimber_elements['featured_media'] ) ) :
			bimber_render_entry_featured_media( array(
				'size'          => 'bimber-classic-1of1',
				'class'         => 'entry-featured-media-main g1-size-1of1',
				'use_microdata' => true,
				'use_nsfw'      => false,
				'apply_link'    => false,
				'show_caption'  => true,
				'allow_video'   => in_array( get_post_format(), apply_filters( 'bimber_single_featured_media_allow_video',  array() ) ),
				'allow_gif' => true,
			) );
		endif;
		?>

		<div class="g1-content-narrow g1-typography-xl entry-content" itemprop="articleBody">
      <?php the_content(); ?>
		</div>
		<div class="sidebar-below-keywords">
		<?php if ( is_active_sidebar( 'sidebar-below-keywords' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-below-keywords' ); ?>
		<?php endif; ?>
		</div>
	</div><!-- .todo -->
</article>

<?php
if ( bimber_is_post_flyin_nav_enabled() ) :
	get_template_part( 'template-parts/post/flyin-next-prev' );
endif;
