<?php

namespace SheKnows\Infuse\Contracts;

use SheKnows\Infuse\Core\Container;

interface SubsystemLoader {
	/**
	 * Constructor.
	 *
	 * @param Container $container A reference to the plugin's IoC container.
	 */
	public function __construct( Container $container );

	/**
	 * The subsystem's loader/bootstrapper method.
	 *
	 * @return void
	 */
	public function load();

}
