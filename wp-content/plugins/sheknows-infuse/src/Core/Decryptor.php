<?php

namespace SheKnows\Infuse\Core;

class Decryptor {

	const SHARED_KEY_OPTION = 'infuse_shared_key';
	const CIPHER            = 'AES-256-CBC';
	const HASHING_ALGORITHM = 'sha256';
	const HASH_LENGTH       = 32;

	/**
	 * @var Options
	 */
	private $options;

	/**
	 * Decryptor constructor.
	 *
	 * @param Options $options
	 */
	public function __construct( Options $options ) {
		$this->options = $options;
	}

	/**
	 * @param string $data
	 *
	 * @return bool|string
	 */
	public function decrypt( $data ) {
		$sharedKey = $this->options->get( self::SHARED_KEY_OPTION );
		if ( ! empty( $sharedKey ) ) {
			$decrypted = $this->decryptAndVerify( $data, $sharedKey );
			if ( $decrypted ) {
				return $decrypted;
			} else {
				$this->options->set( self::SHARED_KEY_OPTION, null );
			}
		}

		return false;
	}

	/**
	 * @param $data
	 * @param $sharedKey
	 *
	 * @return bool|string
	 */
	private function decryptAndVerify( $data, $sharedKey ) {
		// phpcs:disable WordPress.PHP.DiscouragedPHPFunctions
		$encrypted = base64_decode( $data );
		$key       = base64_decode( $sharedKey );
		// phpcs:enable

		$ivLen        = openssl_cipher_iv_length( self::CIPHER );
		$iv           = substr( $encrypted, 0, $ivLen );
		$encryptedRaw = substr( $encrypted, $ivLen + self::HASH_LENGTH );

		// PHPCompatibility doesn't support specifying patch levels, only major
		// and minor versions.  We say we support PHP 5.3, but we technically
		// only support PHP 5.3.3+, as this was added in PHP 5.3.3, but we have
		// to ignore it here to retain the rest of the PHP <5.4 support checks.
		// phpcs:ignore PHPCompatibility.FunctionUse.NewFunctionParameters.openssl_decrypt_ivFound
		$decrypted = openssl_decrypt( $encryptedRaw, self::CIPHER, $key, 1, $iv );

		$calcHmac = hash_hmac( self::HASHING_ALGORITHM, $encryptedRaw, $key, true );
		$hmac     = substr( $encrypted, $ivLen, self::HASH_LENGTH );

		if ( $this->hashCompare( $hmac, $calcHmac ) ) {
			return $decrypted;
		}

		return false;
	}

	/**
	 * @param $actual
	 * @param $expected
	 *
	 * @return bool
	 */
	private function hashCompare( $actual, $expected ) {
		if ( function_exists( 'hash_equals' ) ) {
			return hash_equals( $actual, $expected );
		}

		if ( ! is_string( $actual ) || ! is_string( $expected ) ) {
			return false;
		}

		$len = strlen( $actual );
		if ( strlen( $expected ) !== $len ) {
			return false;
		}

		$status = 0;
		for ( $i = 0; $i < $len; $i ++ ) {
			$status |= ord( $actual[ $i ] ) ^ ord( $expected[ $i ] );
		}

		return 0 === $status;
	}
}
