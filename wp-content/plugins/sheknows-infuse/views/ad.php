<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div id="<?php echo esc_attr( $divid ); ?>" class="sheknows-infuse-ad <?php echo esc_attr( $classes ); ?>" style="<?php echo esc_attr( $style ); ?>"></div>
<script type="text/javascript">
	blogherads.adq.push(function() {
		<?php // phpcs:ignore WordPress.Security.EscapeOutput ?>
		blogherads.defineSlot(<?php echo SheKnows\Infuse\json_encode( $type ); ?>, <?php echo SheKnows\Infuse\json_encode( $divid ); ?>).display();
	});
</script>
