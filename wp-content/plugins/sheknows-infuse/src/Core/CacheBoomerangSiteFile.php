<?php

namespace SheKnows\Infuse\Core;

/**
 * Retrieves and caches a file from the configured Boomerang path.
 *
 * @package SheKnows\Infuse\Core
 */
abstract class CacheBoomerangSiteFile extends CacheBoomerangFile {
	/**
	 * {@inheritdoc}
	 */
	protected function getBaseUrl() {
		$boomerangPath = $this->options->get( 'boomerang_path' );

		if ( empty( $boomerangPath ) ) {
			return false;
		}

		return parent::getBaseUrl() . '/' . trim( $boomerangPath, '/' );
	}

	/**
	 * Return a transient name that's based on the boomerang_path, so that, if
	 * the path ever changes, the name of the transient will change with it.
	 *
	 * @param  boolean  $backoff  True if this is the backoff transient.
	 * @return string
	 */
	protected function transientName( $backoff = false ) {
		$name = parent::transientName( $backoff );

		$name = sprintf( '%s_%s', $name, md5( $this->options->get( 'boomerang_path' ) ) );

		// WP versions prior to 4.4 have a option name limit of 64 characters.
		// Our transient name, with the md5 hash, overruns this.  So, on older
		// versions, we need to truncate the name to fit properly.
		$wpVersion = explode( '.', get_bloginfo( 'version' ) );

		// Handle a version_compare defiency when comparing version strings with
		// only major and minor portions to strings that also include a revision
		// portion.
		if ( ! isset( $wpVersion[2] ) ) {
			$wpVersion[2] = '0';
		}

		if ( version_compare( '4.4.0', implode( '.', $wpVersion ) ) === 1 ) {
			$name = substr( $name, 0, 45 );
		}

		return $name;
	}
}
