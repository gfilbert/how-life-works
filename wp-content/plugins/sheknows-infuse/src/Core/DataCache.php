<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Contracts\CachedDataProvider;

/**
 * Caches data based on responses from a data provider function.
 *
 * @package SheKnows\Infuse\Core
 */
class DataCache {
	/**
	 * The CachedDataProvider that will generate the data to be cached by this instance.
	 *
	 * @var CachedDataProvider
	 */
	protected $dataProvider;

	/**
	 * The arguments that will get passed to the CachedDataProvider::generateData
	 * method when the data needs to be generated.
	 *
	 * @var array
	 */
	protected $dataProviderArguments;

	/**
	 * The cache TTL (in seconds) for successful data generation responses.
	 *
	 * @var int
	 */
	protected $cacheTTL;

	/**
	 * The cache TTL (in seconds) for unsuccessful data generation responses.
	 * If this is null, the $cacheTTL will be used for failures as well.
	 *
	 * @var int|null
	 */
	protected $failCacheTTL;

	/**
	 * The default data to cache if the data generation call fails and does not
	 * return valid data.
	 *
	 * @var mixed
	 */
	protected $defaultFailData;

	/**
	 * The default data to return if data is not ready yet while data is refreshing
	 *
	 * @var mixed
	 */
	protected $defaultDataNotReady;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * The generated transient name to use for storing the cached data, cached
	 * here during the first call to transientName().
	 *
	 * @var string
	 */
	private $transientName;

	/**
	 * @constructor
	 * @param CachedDataProvider $dataProvider
	 * @param array              $dataProviderArguments
	 * @param ErrorLogging       $errorLogging
	 * @param int                $cacheTTL
	 * @param int|null           $failCacheTTL
	 * @param mixed|null         $defaultFailData
	 * @param mixed|null         $defaultDataNotReady
	 */
	public function __construct( CachedDataProvider $dataProvider, $dataProviderArguments, ErrorLogging $errorLogging, $cacheTTL, $failCacheTTL = null, $defaultFailData = null, $defaultDataNotReady = null ) {
		$this->dataProvider          = $dataProvider;
		$this->dataProviderArguments = $dataProviderArguments;
		$this->errorLogging          = $errorLogging;
		$this->cacheTTL              = $cacheTTL;
		$this->failCacheTTL          = $failCacheTTL;
		$this->defaultFailData       = $defaultFailData;
		$this->defaultDataNotReady   = $defaultDataNotReady;
	}

	/**
	 * Get the cached data if present, otherwise generate the data, cache it, then return it.
	 *
	 * @return mixed
	 */
	public function get() {
		$data = get_transient( $this->transientName() );

		if ( false === $data ) {
			if ( ! isset( $this->defaultDataNotReady ) ) {
				$data = $this->fetchData();
			} else {
				$data = $this->defaultDataNotReady;
				$this->scheduleFetch();
			}
		}

		return $data;
	}

	/**
	 * Call the CachedDataProvider to generate the data and cache it.
	 *
	 * @return mixed
	 */
	public function fetchData() {
		try {
			$response = call_user_func( array( $this->dataProvider, 'generateData' ), $this->dataProviderArguments );

			if ( ! ( is_array( $response ) && isset( $response['data'] ) && isset( $response['success'] ) ) ) {
				throw new \Exception( 'Invalid CachedDataProvider response received, must include data and success keys.' );
			}
		} catch ( \Exception $exception ) {
			$this->errorLogging->record( $exception );

			$response = array(
				'success' => false,
				'data'    => $this->defaultFailData,
			);
		}

		$this->updateCache( $response );

		return $response['data'];
	}

	/**
	 * Return a transient name that's based on the class name of the data
	 * provider and the arguments that are passed to it.
	 *
	 * @return string
	 */
	protected function transientName() {
		if ( ! isset( $this->transientName ) ) {
			// phpcs:ignore WordPress.PHP.DiscouragedPHPFunctions
			$key  = md5( get_class( $this->dataProvider ) . ':' . serialize( $this->dataProviderArguments ) );
			$name = 'sheknows_infuse_cache_' . $key;

			// WP versions prior to 4.4 have a option name limit of 64 characters.
			// Our transient name, with the md5 hash, overruns this.  So, on older
			// versions, we need to truncate the name to fit properly.
			$wpVersion = explode( '.', get_bloginfo( 'version' ) );

			// Handle a version_compare defiency when comparing version strings with
			// only major and minor portions to strings that also include a revision
			// portion.
			if ( ! isset( $wpVersion[2] ) ) {
				$wpVersion[2] = '0';
			}

			if ( version_compare( '4.4.0', implode( '.', $wpVersion ) ) === 1 ) {
				$name = substr( $name, 0, 45 );
			}

			$this->transientName = $name;
		}

		return $this->transientName;
	}

	/**
	 * Update the cache content using cache ttl base on if content failed or succeed
	 * @param array $response
	 */
	protected function updateCache( $response ) {
		$ttl = $this->cacheTTL;
		if ( ! $response['success'] && ! empty( $this->failCacheTTL ) ) {
			$ttl = $this->failCacheTTL;
		}
		set_transient( $this->transientName(), $response['data'], $ttl );
	}

	/**
	 * Schedule the data fetch during shutdown event
	 */
	protected function scheduleFetch() {
		// Attached to shutdown event to refresh cache
		add_action( 'shutdown', array( $this, 'fetchData' ) );
	}

}
