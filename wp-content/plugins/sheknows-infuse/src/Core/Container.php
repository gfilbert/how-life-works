<?php

namespace SheKnows\Infuse\Core;

/**
 * Class Container
 * A simple IoC container for dependency injection.
 *
 * @package SheKnows\Infuse\Core
 */
class Container {

	/**
	 * Hold all of our registered bindings.
	 *
	 * @var array
	 */
	protected $bindings = array();

	/**
	 * Hold all of our registered callout classes.
	 *
	 * @var array
	 */
	protected $callouts = array();

	/**
	 * Normalize the class names that get passed into the Container so we can
	 * account for minor mismatches.
	 *
	 * @param $name
	 *
	 * @return string
	 */
	protected function normalizeClassName( $name ) {
		return ltrim( $name, '\\' );
	}

	/**
	 * Bind a callback to a request for a given fully qualified class name.  The
	 * class can also be registered as a "callout" class that other classes can
	 * look up to find implementations (ie of Admin classes in subsystems).
	 *
	 * @param string   $class
	 * @param callable $maker
	 * @param mixed   $callout
	 */
	public function bind( $class, $maker, $callout = null ) {
		if ( is_callable( $maker ) ) {
			$class = $this->normalizeClassName( $class );

			$this->bindings[ $class ] = array(
				'maker' => $maker,
			);

			if ( isset( $callout ) ) {
				if ( ! isset( $this->callouts[ $callout ] ) ) {
					$this->callouts[ $callout ] = array();
				}

				$this->callouts[ $callout ][] = $class;
			}
		}
	}

	/**
	 * Retrieve the instance of a bound class name.
	 *
	 * @param string $class
	 *
	 * @return mixed
	 */
	public function resolve( $class ) {
		$class = $this->normalizeClassName( $class );

		if ( ! isset( $this->bindings[ $class ] ) ) {
			return false;
		}

		if ( ! isset( $this->bindings[ $class ]['object'] ) ) {
			// Pass in the Container so that dependencies can be resolved and
			// and injected into instantiated objects.
			$this->bindings[ $class ]['object'] = call_user_func( $this->bindings[ $class ]['maker'], $this );
		}

		return $this->bindings[ $class ]['object'];
	}

	/**
	 * Get all of the class names that have registered as a given callout.
	 * @param  string $callout
	 *
	 * @return array
	 */
	public function getCallouts( $callout ) {
		if ( isset( $this->callouts[ $callout ] ) ) {
			return $this->callouts[ $callout ];
		}

		return array();
	}
}
