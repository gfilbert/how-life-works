<?php
// Prevent direct script access
if ( !defined( 'ABSPATH' ) )
	die ( 'No direct script access allowed' );

/**
* Child Theme Setup
* 
* Always use child theme if you want to make some custom modifications. 
* This way theme updates will be a lot easier.
*/
function bimber_childtheme_setup() {
}

add_action( 'after_setup_theme', 'bimber_childtheme_setup' );



//Adding adsense ad unit inside the article
//Insert ads after every third paragraph of single post content.

add_filter( 'the_content', 'prefix_insert_post_ads' );

function prefix_insert_post_ads( $content ) {
	//CHANGE BELOW AdSense CODE WITH YOUR OWN CODE
	$ad_code = '[xyz-ips snippet="sidebar-repeat-4-paragraphs"]	
<p></p>
';
	
    if ( is_single() ) {
	//CHANGE TO DESIRED NUMBER YOU WANT ADVERT TO BE APPEARED
        return prefix_insert_after_paragraph( $ad_code, 4, $content );
    }

    return $content;
}

// Parent Function that makes the magic happen

function prefix_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
    $closing_p = '</p>';
    $paragraphs = explode( $closing_p, $content );
    foreach ($paragraphs as $index => $paragraph) {

        if ( trim( $paragraph ) ) {
            $paragraphs[$index] .= $closing_p;
        }


        if ( ( ($index + 1) % $paragraph_id ) == 0 ) {
            $paragraphs[$index] .= $insertion;
        }
	    
    }

    return implode( '', $paragraphs );


}


function my_custom_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Repeat 4 Paragraphs', 'Bimber-child' ),
            'id' => 'sidebar-repeat-4-paragraphs',
            'description' => __( 'This sidebar appears below post content.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Below Content', 'Bimber-child' ),
            'id' => 'sidebar-below-content',
            'description' => __( 'This sidebar appears below articles on keyword posts.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Below Article on KW Posts', 'Bimber-child' ),
            'id' => 'sidebar-below-keywords',
            'description' => __( 'This sidebar appears below articles on keyword posts.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'Custom AdAce', 'Bimber-child' ),
            'id' => 'sidebar-custom-adace',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-01', 'Bimber-child' ),
            'id' => 'shemedia-01',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-02', 'Bimber-child' ),
            'id' => 'shemedia-02',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-03', 'Bimber-child' ),
            'id' => 'shemedia-03',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-04', 'Bimber-child' ),
            'id' => 'shemedia-04',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-05', 'Bimber-child' ),
            'id' => 'shemedia-05',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-06', 'Bimber-child' ),
            'id' => 'shemedia-06',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-07', 'Bimber-child' ),
            'id' => 'shemedia-07',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-08', 'Bimber-child' ),
            'id' => 'shemedia-08',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-09', 'Bimber-child' ),
            'id' => 'shemedia-09',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-10', 'Bimber-child' ),
            'id' => 'shemedia-10',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-11', 'Bimber-child' ),
            'id' => 'shemedia-11',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-11', 'Bimber-child' ),
            'id' => 'shemedia-11',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-11', 'Bimber-child' ),
            'id' => 'shemedia-11',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-12', 'Bimber-child' ),
            'id' => 'shemedia-12',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-13', 'Bimber-child' ),
            'id' => 'shemedia-13',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-14', 'Bimber-child' ),
            'id' => 'shemedia-14',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-15', 'Bimber-child' ),
            'id' => 'shemedia-15',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-16', 'Bimber-child' ),
            'id' => 'shemedia-16',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-17', 'Bimber-child' ),
            'id' => 'shemedia-17',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-18', 'Bimber-child' ),
            'id' => 'shemedia-18',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-19', 'Bimber-child' ),
            'id' => 'shemedia-19',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-20', 'Bimber-child' ),
            'id' => 'shemedia-20',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-21', 'Bimber-child' ),
            'id' => 'shemedia-21',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-22', 'Bimber-child' ),
            'id' => 'shemedia-22',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-23', 'Bimber-child' ),
            'id' => 'shemedia-23',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-24', 'Bimber-child' ),
            'id' => 'shemedia-24',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-25', 'Bimber-child' ),
            'id' => 'shemedia-25',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-26', 'Bimber-child' ),
            'id' => 'shemedia-26',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-27', 'Bimber-child' ),
            'id' => 'shemedia-27',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-28', 'Bimber-child' ),
            'id' => 'shemedia-28',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-29', 'Bimber-child' ),
            'id' => 'shemedia-29',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-30', 'Bimber-child' ),
            'id' => 'shemedia-30',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-31', 'Bimber-child' ),
            'id' => 'shemedia-31',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-31', 'Bimber-child' ),
            'id' => 'shemedia-31',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-32', 'Bimber-child' ),
            'id' => 'shemedia-32',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-33', 'Bimber-child' ),
            'id' => 'shemedia-33',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-34', 'Bimber-child' ),
            'id' => 'shemedia-34',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-35', 'Bimber-child' ),
            'id' => 'shemedia-35',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-36', 'Bimber-child' ),
            'id' => 'shemedia-36',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-37', 'Bimber-child' ),
            'id' => 'shemedia-37',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-38', 'Bimber-child' ),
            'id' => 'shemedia-38',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-39', 'Bimber-child' ),
            'id' => 'shemedia-39',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'shemedia-40', 'Bimber-child' ),
            'id' => 'shemedia-40',
            'description' => __( 'This sidebar appears in custom placements only.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array (
            'name' => __( 'greenlumber-below-content', 'Bimber-child' ),
            'id' => 'greenlumber-below-content',
            'description' => __( 'This sidebar appears below the content on the Green Lumber advertorial.', 'Bimber-child' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'my_custom_sidebar' );

function myFilter($query) {
if ($query->is_feed) {
$query->set('cat','-298');
}

return $query;
}

add_filter(‘pre_get_posts’,’myFilter’);