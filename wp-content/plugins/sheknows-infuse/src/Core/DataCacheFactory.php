<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Contracts\CachedDataProvider;

/**
 * Instantiates and returns instances of the DataCache class
 *
 * Class DataCacheFactory
 *
 * @package SheKnows\Infuse\Core
 */
class DataCacheFactory {
	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * @param ErrorLogging $errorLogging
	 */
	public function __construct( ErrorLogging $errorLogging ) {
		$this->errorLogging = $errorLogging;
	}

	/**
	 * Create and return a new DataCache instance.
	 *
	 * @param CachedDataProvider $dataProvider
	 * @param array              $dataProviderArguments
	 * @param int                $cacheTTL
	 * @param int|null           $failCacheTTL
	 * @param mixed|null         $defaultFailData
	 * @param mixed|null         $defaultDataNotReady
	 */
	public function make( CachedDataProvider $dataProvider, $dataProviderArguments, $cacheTTL, $failCacheTTL = null, $defaultFailData = null, $defaultDataNotReady = null ) {
		return new DataCacheUsingPostMeta( $dataProvider, $dataProviderArguments, $this->errorLogging, $cacheTTL, $failCacheTTL, $defaultFailData, $defaultDataNotReady );
	}
}
