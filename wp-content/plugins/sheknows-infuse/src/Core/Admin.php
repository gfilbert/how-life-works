<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Plugin;

/**
 * Presently works with Options to create and populate the settings form for
 * Infuse. Will ultimately work with subsystems to construct the form.
 *
 * Class Admin
 *
 * @package SheKnows\Infuse\Core
 */
class Admin {
	/**
	 * The setting name for the Boomerang path.
	 *
	 * @var string
	 */
	const OPTION_BOOMERANG_PATH = 'boomerang_path';

	/**
	 * The setting name for the infuse Shared Key.
	 *
	 * @var string
	 */
	const OPTION_INFUSE_SHARED_KEY = 'infuse_shared_key';

	/**
	 * The autoconfig link.
	 */
	const AUTOCONFIG_LINK = 'https://infuse-autoconfig.shemedia.com';

	/**
	 * Name of nonce to verify callbacks from Autoconfig Service are authentic.
	 */
	const NONCE_NAME = 'sheknows_infuse_autoconfig_callback';

	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var ViewFactory
	 */
	protected $viewFactory;

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @var UpstreamSiteConfig
	 */
	protected $siteConfig;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * Caches the list of all of the Admin classes that exist in Subsystems.
	 *
	 * @var array
	 */
	protected $subsystemAdminClasses;

	/**
	 * Stores the option values that failed validation so they can be saved in
	 * the transient at shutdown and used when the form is rendered at the next
	 * page load.
	 *
	 * @var array
	 */
	protected $validationFailures = array();

	/**
	 * Stores validation failures that were retrieved from the transient saved
	 * during the previous save attempt.  If an option has a value here, it will
	 * be used to populate the form field's default value instead of the
	 * currently-saved option so that users can make adjustments to the data
	 * they tried to save instead of having to start over again.
	 *
	 * @var array
	 */
	protected $priorValidationFailures = array();

	/**
	 * Keeps track of whether the shutdown hook has been registered.
	 *
	 * @var boolean
	 */
	protected $shutdownRegistered = false;

	/**
	 * Error message from Autoconfig Service
	 *
	 * @var mixed
	 */
	protected $autoConfigError = '';

	/**
	 * @param Options $options
	 * @param Plugin $plugin
	 * @param ViewFactory $viewFactory
	 * @param Container $container
	 * @param UpstreamSiteConfig $siteConfig
	 * @param ErrorLogging       $errorLogging
	 */
	public function __construct( Options $options, Plugin $plugin, ViewFactory $viewFactory, Container $container, UpstreamSiteConfig $siteConfig, ErrorLogging $errorLogging ) {
		$this->options      = $options;
		$this->plugin       = $plugin;
		$this->viewFactory  = $viewFactory;
		$this->container    = $container;
		$this->siteConfig   = $siteConfig;
		$this->errorLogging = $errorLogging;
	}

	/**
	 * Display the options page.
	 */
	public function displaySettingsPage() {
		$this->retrievePriorValidationFailures();

		try {
			if ( $this->checkRedirectFromAutoconfig() ) {
				add_settings_error( 'general', 'settings_updated', __( 'Infuse has been successfully configured for your site.', 'sheknows-infuse' ), 'updated' );

				// This is called in the options page header before we get to
				// this point, so we have to call it again here for the notice
				// to appear.
				settings_errors();

				// If we got a callback, then we need to re-register our
				// settings too, since they would have been disabled from the
				// first run.
				$this->registerSettings();
			}
		} catch ( \Exception $exception ) {
			$this->autoConfigError = '<p class="red-text">There was an error retrieving your configuration. Please try again. If you continue to experience problems, please <a href="https://www.shemedia.com/support/#ask-question" target="_blank">contact the Helpdesk</a>.</p>';
			$this->errorLogging->record( $exception );
		}

		$boomerangPath = $this->options->get( self::OPTION_BOOMERANG_PATH );

		$vars = array(
			'logo'            => plugin_dir_url( $this->plugin->getFile() ) . 'public/images/logo.png',
			'slug'            => $this->plugin->getSlug(),
			'settings_fields' => $this->options->getStoredName(),
			'errors'          => $this->errorLogging->get(),
			'boomerang_path'  => $boomerangPath,
			'autoconfig_link' => $this->buildAutoconfigLink( $boomerangPath ),
		);

		// phpcs:ignore WordPress.Security.EscapeOutput
		echo $this->viewFactory->make( 'admin/options', $vars )->render();
	}

	/**
	 * Register a setting with WordPress.
	 *
	 * @param string   $name              The name of the setting.  This will be
	 *                                    namespaced with the plugin's slug when
	 *                                    stored in the database.
	 * @param callback $sanitizeCallback  An optional callback to sanitize the
	 *                                    input.
	 */
	public function registerSetting( $name, $sanitizeCallback = null ) {
		$group      = $this->options->getStoredName();
		$storedName = $this->options->getStoredName( $name );

		register_setting( $group, $storedName, $sanitizeCallback );
	}

	/**
	 * Add settings section to the Options page.
	 *
	 * @param string   $name      The slug/name of the section.
	 * @param string   $title     The title of the section.
	 * @param callable $callback  An optional callback to display extra details
	 *                            about the section.
	 */
	public function addSettingsSection( $name, $title, $callback = '__return_false' ) {
		$slug = $this->plugin->getSlug();

		add_settings_section( $name, $title, $callback, $slug );
	}

	/**
	 * Add a settings field to the Options page.
	 *
	 * @param string   $id                   The DOM id of the option.
	 * @param string   $section              The slug of the section this field
	 *                                       belongs to.
	 * @param string   $label                The label for the field.
	 * @param callback $inputMarkupCallback  A callback that provides the markup
	 *                                       for the input field itself.
	 */
	public function addSettingsField( $id, $section, $label, $inputMarkupCallback ) {
		$slug = $this->plugin->getSlug();
		$args = array(
			'label_for' => $id,
		);

		add_settings_field( $id, $label, $inputMarkupCallback, $slug, $section, $args );
	}

	/**
	 * Get the name attribute for the input tag for a given settings field.
	 *
	 * @param  string  $name        The non-namespaced name of the option.
	 * @param  boolean $lazyLoaded  Whether the option is lazy loaded.
	 * @return string               The computed name attribute value.
	 */
	public function getInputName( $name, $lazyLoaded = false ) {
		if ( $lazyLoaded ) {
			return $this->options->getStoredName( $name );
		} else {
			return $this->options->getStoredName() . "[$name]";
		}
	}

	/**
	 * Populate the options page form.
	 */
	public function registerSettings() {
		// Register our core eagerly-loaded setting where most options live.
		$this->registerSetting( null, array( $this, 'sanitize' ) );

		// Set up our core General section and boomerang_path option.
		$this->addSettingsSection( 'general', 'General' );
		$this->addSettingsField(
			self::OPTION_BOOMERANG_PATH,
			'general',
			'Boomerang path <span class="red-text">*</span>',
			array( $this, 'boomerangPathMarkup' )
		);

		// Allow the subssystems to register their fields/sections/settings.
		foreach ( $this->getSubsystemAdminClasses() as $class ) {
			$this->container->resolve( $class )->register();
		}
	}

	/**
	 * Return a list of all of the Admin callout classes registered by subsystems.
	 *
	 * @return array  An array of the class names.
	 */
	protected function getSubsystemAdminClasses() {
		return $this->container->getCallouts( 'SubsystemAdmin' );
	}

	/**
	 * Get the name of the transient that holds values from failed form
	 * validations.
	 *
	 * @return string
	 */
	protected function getValidationFailuresTransientName() {
		return $this->options->getStoredName() . '_validation_failures_' . get_current_user_id();
	}

	/**
	 * Called in a shutdown hook to save the values from failed validations in a
	 * transient for use in the next render of the form.
	 */
	public function saveValidationFailures() {
		$transientName = $this->getValidationFailuresTransientName();

		if ( ! empty( $this->validationFailures ) ) {
			// Similar to WordPress' handling of settings errors, expire this
			// very quickly, no more than 30 seconds.
			set_transient( $transientName, $this->validationFailures, 30 );
		} else {
			delete_transient( $transientName );
		}
	}

	/**
	 * Retrieve the values from the transient that stored any previous failed
	 * validations.
	 */
	protected function retrievePriorValidationFailures() {
		$transientName = $this->getValidationFailuresTransientName();

		$failures = get_transient( $transientName );

		if ( is_array( $failures ) ) {
			$this->priorValidationFailures = $failures;
			delete_transient( $transientName );
		}
	}

	/**
	 * Get the default value to use for the setting's form field by first
	 * checking to see if a previous submission attempt resulted in a validation
	 * failure and, if so, using the failed value so the user can correct the
	 * error condition rather than having to start over.  If there was no prior
	 * submission, or the prior submission did not result in a validation error
	 * for the given setting, then return the currently stored value (or the
	 * default if not yet stored).
	 *
	 * @param  string $setting  The name of the setting.
	 * @return mixed            The value.
	 */
	public function getDefaultValue( $setting ) {
		if ( isset( $this->priorValidationFailures[ $setting ] ) ) {
			return $this->priorValidationFailures[ $setting ];
		} else {
			return $this->options->get( $setting );
		}
	}

	/**
	 * Handle a validation error by displaying a message to the user and storing
	 * the failed value so the user can edit it on the next form render instead
	 * of having to start over from the old saved option.
	 *
	 * @param  string $setting The setting name.
	 * @param  string $code    The code to use with add_settings_error.
	 * @param  string $message The validation error message to display.
	 * @param  mixed  $value   The value that failed validation.
	 */
	public function validationError( $setting, $code, $message, $value ) {
		add_settings_error( $setting, $code, $message );
		$this->validationFailures[ $setting ] = $value;

		if ( ! $this->shutdownRegistered ) {
			add_action( 'shutdown', array( $this, 'saveValidationFailures' ) );
		}
	}

	/**
	 * Validate and clean up submissions.
	 *
	 * @param  array  $input
	 * @return array
	 */
	public function sanitize( $input ) {
		$newInput = array();

		$newInput[ self::OPTION_BOOMERANG_PATH ]    = $this->options->get( self::OPTION_BOOMERANG_PATH );
		$newInput[ self::OPTION_INFUSE_SHARED_KEY ] = $this->options->get( self::OPTION_INFUSE_SHARED_KEY );

		// Now allow the subsystems to sanitize their options.
		foreach ( $this->getSubsystemAdminClasses() as $class ) {
			$sanitized = $this->container->resolve( $class )->sanitize( $input );
			$newInput  = array_merge( $newInput, $sanitized );
		}

		return $newInput;
	}

	/**
	 * Get the list of one-time query args used by the settings page so they
	 * can be filtered out by the canonical URL rewriter.
	 */
	public function getOneTimeQueryArgs() {
		return array( 'nonce', 'infusepasspath' );
	}

	/**
	 * Fetches data from Autoconfig Service when infusepasspath query param is set. If
	 * successful after a nonce check, sets needed options. If not
	 * throws an error.
	 *
	 * @return boolean
	 * @throws \Exception
	 */
	protected function checkRedirectFromAutoconfig() {
		// If there isn't an infusepasspath query param this is not an autoconfig service
		// redirect.
		if ( ! isset( $_GET['infusepasspath'] ) ) {
			return false;
		}

		// We need to forcefully strip our one-time params from the REQUEST_URI,
		// otherwise, if the user immediately saves the form after the autoconfig
		// completes, it will redirect us back to this page with the query params
		// still attached, causing us to retry (and fail) the autoconfig procedure.
		$_SERVER['REQUEST_URI'] = remove_query_arg( array( 'infusepasspath', 'nonce' ) );

		// Verify nonce set in boomerangPathMarkup matches nonce query param.
		if ( ! wp_verify_nonce( $_GET['nonce'], self::NONCE_NAME ) ) {
			throw new \Exception( 'Invalid callback from autoconfig service: nonce does not match' );
		}

		$responseJSON = $this->makeAutoconfigCall( $_GET['infusepasspath'] );

		if ( ! ( isset( $responseJSON['encryptionkey'] ) && isset( $responseJSON['boomerangpath'] ) ) ) {
			throw new \Exception( 'Invalid response from autoconfig service: not all configuration keys were present.' );
		}

		$this->options->set( self::OPTION_BOOMERANG_PATH, $responseJSON['boomerangpath'] );
		$this->options->set( self::OPTION_INFUSE_SHARED_KEY, $responseJSON['encryptionkey'] );

		// Force retrieve data from upstream config.
		$this->siteConfig->forceRetrieve();

		return true;
	}

	/**
	 * Performs a call to Autoconfig.
	 *
	 * @param $infusePassPath
	 * @return array|mixed|object
	 * @throws \Exception
	 */
	protected function makeAutoconfigCall( $infusePassPath ) {
		$autoconfigURL = self::AUTOCONFIG_LINK . '/api/callback/' . rawurlencode( $infusePassPath );
		$response      = wp_remote_get( $autoconfigURL );
		if ( is_wp_error( $response ) ) {
			throw new \Exception( $response->get_error_message() );
		}

		$body = wp_remote_retrieve_body( $response );
		if ( $body ) {
			$decoded = json_decode( $body, true );

			if ( is_array( $decoded ) ) {
				return $decoded;
			}
		}

		throw new \Exception( 'Invalid data returned from autoconfig request.' );
	}

	/**
	 * Populates form elements for Boomerang path.
	 */
	public function boomerangPathMarkup() {
		$boomerangPath  = $this->options->get( self::OPTION_BOOMERANG_PATH );
		$sharedKey      = $this->options->get( self::OPTION_INFUSE_SHARED_KEY );
		$autoConfigLink = $this->buildAutoconfigLink();

		if ( empty( $boomerangPath ) ) {
			// Infuse isn't configured.
			$pathRemark = '<p class="red-text"><a href="' . esc_url( $autoConfigLink ) . '">Click here</a> to automatically configure Infuse. If you are not currently logged in to your Partner Network account, you will be asked to log in.</p>';
		} else {
			$pathRemark = sprintf( '<p><strong><code>%s</code></strong></p>', esc_html( $boomerangPath ) );

			if ( empty( $sharedKey ) ) {
				// Infuse is configured with the Boomerang path but is missing
				// the shared key.  In this case, we send the ad path along
				// in the autoconfig link.
				$autoConfigLink = $this->buildAutoconfigLink( $boomerangPath );

				$pathRemark .= '<p class="red-text">SHE Media Infuse is not fully configured. Please <a href="' . esc_url( $autoConfigLink ) . '">click here</a> to complete the configuration.</p>';
			}
		}

		if ( ! empty( $this->autoConfigError ) ) {
			$pathRemark .= $this->autoConfigError;
		}

		echo $pathRemark; // phpcs:ignore WordPress.Security.EscapeOutput
	}

	/**
	 * Builds the link to autoconfig service.
	 *
	 * @param string $adPath
	 * @return string
	 */
	public function buildAutoconfigLink( $adPath = '' ) {
		// Sets a nonce for verification that redirects from Autoconfig Service are
		// authentic. Checked in checkForPathFromAutoconfig.
		$nonce = wp_create_nonce( self::NONCE_NAME );

		$autoConfigLink  = self::AUTOCONFIG_LINK . ( ! empty( $adPath ) ? '/key' : '' );
		$currentPath     = $this->plugin->getOptionsPageUrl();
		$currentPath    .= strpos( $currentPath, '?' ) === false ? '?' : '&';
		$currentPath    .= 'nonce=' . $nonce;
		$autoConfigLink .= '?callback_uri=' . rawurlencode( $currentPath );

		if ( ! empty( $adPath ) ) {
			$autoConfigLink .= '&ad_path=' . rawurlencode( $adPath );
		}

		return $autoConfigLink;
	}

	/**
	 * Helper to let subsystems disable their form inputs when the basic
	 * setting(s) have not yet been configured.
	 *
	 * @return boolean  True if all necessary settings are configured.
	 */
	public function globallyEnabled() {
		$boomerangPath = $this->options->get( self::OPTION_BOOMERANG_PATH );

		return ! empty( $boomerangPath );
	}
}
