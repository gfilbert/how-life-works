<?php

namespace SheKnows\Infuse\Subsystems\AdsTxt;

use SheKnows\Infuse\Core\Options;
use SheKnows\Infuse\Core\SubsystemAdmin;
use SheKnows\Infuse\Core\PluginsHelper;
use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Core\Admin as CoreAdmin;
use SheKnows\Infuse\Core\UpstreamCommonConfig;
use SheKnows\Infuse\Core\ErrorLogging;

class Admin extends SubsystemAdmin {
	/**
	 * The name of the option that enables ads.txt serving.
	 *
	 * @var string
	 */
	const OPTION_ENABLED = 'adstxt_enabled';

	/**
	 * The name of the option that holds extra ads.txt entries.
	 *
	 * @var string
	 */
	const OPTION_EXTRA_LINES = 'adstxt_extra_lines';

	/**
	 * The name of the option that lets the admin delete an ads.txt file.
	 * NOTE: This option is not actually stored in the database.
	 *
	 * @var string
	 */
	const OPTION_DELETE_FILE = 'adstxt_delete_file';

	/**
	 * Regex to validate domains in ads.txt.
	 */
	const DOMAIN_REGEX = '/^((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+((xn--){1}[a-z0-9]{1,59}|[a-z]{2,63})$/i';

	/**
	 * Caches whether permalinks are enabled so we only have to call the API
	 * once.
	 *
	 * @var boolean
	 */
	protected $permalinksEnabled;

	/**
	 * Caches whether a physical ads.txt file was discovered.
	 *
	 * @var boolean
	 */
	protected $adsTxtFileExists;

	/**
	 * @var PluginsHelper
	 */
	protected $pluginsHelper;

	/**
	 * @var UpstreamCommonConfig
	 */
	protected $upstreamCommonConfig;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * @param Options $options
	 * @param Plugin $plugin
	 * @param CoreAdmin $coreAdmin
	 * @param PluginsHelper $pluginsHelper
	 * @param UpstreamCommonConfig $upstreamCommonConfig
	 * @param ErrorLogging $errorLogging
	 */
	public function __construct( Options $options, Plugin $plugin, CoreAdmin $coreAdmin, PluginsHelper $pluginsHelper, UpstreamCommonConfig $upstreamCommonConfig, ErrorLogging $errorLogging ) {
		parent::__construct( $options, $plugin, $coreAdmin );

		$this->pluginsHelper        = $pluginsHelper;
		$this->upstreamCommonConfig = $upstreamCommonConfig;
		$this->errorLogging         = $errorLogging;
	}

	/**
	 * Sanitize the Ads.txt non-lazy-loaded settings.
	 *
	 * @param  array $input  The array of non-lazy-loaded options.
	 * @return array         The sanitized input values from this subsystem.
	 */
	public function sanitize( $input ) {
		$newInput   = array();
		$oldEnabled = $this->options->get( self::OPTION_ENABLED );

		if ( ! $this->coreAdmin->globallyEnabled() ) {
			// If we have no Boomerang path, there was no form, so we just re-
			// store the existing value.
			$newInput[ self::OPTION_ENABLED ] = (bool) $oldEnabled;
		} else {
			if ( $this->checkForPhysicalFile() ) {
				// If an actual ads.txt file exists and the user checked the box
				// to delete the file, try to do that now.
				// NOTE: The delete option is *not* stored in the database.  It
				// is merely a signal to Infuse to attempt to delete the file.
				if ( isset( $input[ self::OPTION_DELETE_FILE ] ) && $input[ self::OPTION_DELETE_FILE ] ) {
					//phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged
					$success = @unlink( ABSPATH . '/ads.txt' );

					if ( ! $success ) {
						add_settings_error( self::OPTION_DELETE_FILE, 'cant_delete_adstxt', 'There was an error removing your <code>ads.txt</code> file.  Please contact Support for assistance removing this file.' );
					} else {
						add_settings_error( self::OPTION_DELETE_FILE, 'adstxt_file_deleted', 'The <code>ads.txt</code> file was successfully deleted.', 'updated' );

						// If the user chose to delete the ads.txt file, and it
						// was successfully deleted, then we can re-enable the
						// subsystem.
						$newInput[ self::OPTION_ENABLED ] = true;
					}
				} else {
					// Disable the subsystem when an ads.txt file exists.
					$newInput[ self::OPTION_ENABLED ] = false;
				}
			} else {
				// Normal processing of the form happens here.  At this point,
				// we have our Boomerang path, and no physical ads.txt file
				// exists.
				if ( isset( $input[ self::OPTION_ENABLED ] ) && $input[ self::OPTION_ENABLED ] ) {
					$newInput[ self::OPTION_ENABLED ] = true;
				} else {
					$newInput[ self::OPTION_ENABLED ] = false;
				}
			}
		}

		return $newInput;
	}

	/**
	 * Register the ads.txt related settings, fields, etc.
	 */
	public function register() {
		$this->coreAdmin->registerSetting( self::OPTION_EXTRA_LINES, array( $this, 'sanitizeAdsTxtLines' ) );

		if ( $this->coreAdmin->globallyEnabled() ) {
			$this->coreAdmin->addSettingsSection( 'ads_txt', 'Ads.txt', array( $this, 'adsTxtSectionHeader' ) );

			// If the site has an actual ads.txt file, don't show the normal
			// config options.  Instead, notify the user and allow them to
			// remove file to enable Infuse's ads.txt functionality.
			if ( $this->checkForPhysicalFile() ) {
				$this->coreAdmin->addSettingsField( self::OPTION_DELETE_FILE, 'ads_txt', 'Delete the ads.txt file', array( $this, 'deleteFile' ) );
			} else {
				$this->coreAdmin->addSettingsField( self::OPTION_ENABLED, 'ads_txt', 'Enable Ads.txt', array( $this, 'adsTxtEnabled' ) );
				$this->coreAdmin->addSettingsField( self::OPTION_EXTRA_LINES, 'ads_txt', 'Custom Ads.txt entries', array( $this, 'adsTxtExtraLines' ) );
			}
		}
	}

	/**
	 * Wrapper for Ads.txt section.
	 */
	public function adsTxtSectionHeader() {
		echo '<p">Ads.txt, or <em>A</em>uthorized <em>D</em>igital <em>S</em>ellers, is an industry standard fraud prevention mechanism that helps advertisers ensure they are buying legitimate advertising inventory.<br />Without Ads.txt, counterfeit sites can masquerade as legitimate sites in advertising exchanges, defrauding legitimate sites of revenue and devaluing the site\'s inventory.</p>';

		// If we're forcefully disabled because of a physical ads.txt file or a
		// missing Boomerang path, then bail here and don't display any of the
		// section header warnings.
		if ( ! $this->coreAdmin->globallyEnabled() || $this->checkForPhysicalFile() ) {
			return;
		}

		// Only display other warnings if the subsystem is enabled.
		if ( $this->options->get( self::OPTION_ENABLED ) ) {
			if ( ! $this->verifyPermalinksAvailable() ) {
				$permalinksUrl = admin_url( 'options-permalink.php' );
				echo '<p class="red-text">Ads.txt support requires that URL rewriting be enabled in your web server.  URL rewriting is usually enabled by <a href="' . esc_url( $permalinksUrl ) . '">choosing a Permalinks style</a>, however Permalinks are currently disabled.  If you receive an error browsing to <a href="' . esc_url( site_url( '/ads.txt' ) ) . '">your ads.txt URL</a>, try <a href="' . esc_url( $permalinksUrl ) . '">enabling Permalinks</a>.</p>';
			}

			$competing = $this->checkForCompetingPlugins();
			if ( ! empty( $competing ) ) {
				$items = sprintf( '<li>%s</li>', implode( '</li><li>', array_map( 'esc_html', $competing ) ) );

				// phpcs:ignore WordPress.Security.EscapeOutput
				printf( '<p class="red-text">It appears that your site has one or more active plugins that may also try to serve Ads.txt.  It is recommended that these plugins <a href="%s">be deactivated</a> to ensure that Infuse is able to serve your SHE Media Ads.txt entries.</p><ul class="red-text competing-plugins-list">Detected plugins:%s</ul>', esc_url( admin_url( 'plugins.php' ) ), $items );
			}
		}
	}

	/**
	 * Adds the checkbox to enable/disable the functionality this subsystem
	 * provides.
	 */
	public function adsTxtEnabled() {
		printf(
			'<input type="checkbox" id="%s" name="%s" value="1" %s /><p><b>Recommended.</b>  If checked, Infuse will serve an ads.txt file for your site that automatically includes all applicable records provided by SHE Media.</p>',
			esc_attr( self::OPTION_ENABLED ),
			esc_attr( $this->coreAdmin->getInputName( self::OPTION_ENABLED ) ),
			checked( $this->coreAdmin->getDefaultValue( self::OPTION_ENABLED ), true, false )
		);
	}

	/**
	 * Renders the form element for ads.txt extra lines.
	 */
	public function adsTxtExtraLines() {
		printf(
			'<textarea id="%s" autocomplete="off" name="%s" %s>%s</textarea><p>Ads.txt entries provided here will be appended to the entries provided by SHE Media when serving your ads.txt file.</p>',
			esc_attr( self::OPTION_EXTRA_LINES ),
			esc_attr( $this->coreAdmin->getInputName( self::OPTION_EXTRA_LINES, true ) ),
			disabled( ! $this->options->get( self::OPTION_ENABLED ), true, false ),
			esc_html( $this->coreAdmin->getDefaultValue( self::OPTION_EXTRA_LINES ) )
		);
	}

	/**
	 * Adds a checkbox to let the admin delete an ads.txt file that might be
	 * present inside the WordPress install.
	 */
	public function deleteFile() {
		printf(
			'<input type="checkbox" id="%s" name="%s" value="1" /><p class="red-text">A file named <code>ads.txt</code> currently exists in the root folder of your WordPress installation.  This file must be removed in order to utilize the Ads.txt features provided by Infuse.  Check this box and click Save to remove the <code>ads.txt</code> file and reactivate the Ads.txt features in Infuse.</p>',
			esc_attr( self::OPTION_DELETE_FILE ),
			esc_attr( $this->coreAdmin->getInputName( self::OPTION_DELETE_FILE ) )
		);
	}

	/**
	 * Cleans any extra ads.txt lines supplied by the user.
	 *
	 * @param  string $value  The Ads.txt entries input by the user.
	 * @return string
	 */
	public function sanitizeAdsTxtLines( $value ) {
		$oldLines = $this->options->get( self::OPTION_EXTRA_LINES );

		if ( isset( $value ) ) {
			$lines     = preg_split( '/\r\n|\r|\n/', $value );
			$sanitized = array();

			foreach ( $lines as $number => $line ) {
				$sanitized[] = $this->validateLine( $line, $number + 1, $value );
			}

			return implode( PHP_EOL, $sanitized );
		} else {
			// If the value is NULL, that means the form field was disabled, so
			// the browser didn't include this form input in the submission, but
			// WordPress still called our sanitize function, because it's still
			// going to save the value, so we need to tell it to reuse what it
			// already has stored.
			return $oldLines;
		}
	}

	/**
	 * Validates ads.txt line.
	 *
	 * @param string $value
	 * @param int $number
	 * @param string $fullValue
	 * @return string
	 */
	private function validateLine( $value, $number, $fullValue ) {
		$split = explode( '#', $value );
		$line  = rtrim( $split[0] );
		if ( empty( $line ) ) {
			// Full-line comment or empty line.
			return $value;
		}

		if ( 1 < strpos( $line, '=' ) ) {
			// KEY=VALUE (variable declaration) string.
			if ( ! preg_match( '/^(CONTACT|SUBDOMAIN)=/i', $line ) ) {
				$this->validationError( 'infuse_ads_txt_invalid_variable', $line, $number, $fullValue );
			} elseif ( 0 === stripos( $line, 'subdomain=' ) ) {
				$subdomain = explode( '=', $line );
				array_shift( $subdomain );

				if ( 1 !== count( $subdomain ) || ! preg_match( self::DOMAIN_REGEX, $subdomain[0] ) ) {
					$subdomain = implode( '=', $subdomain );
					$this->validationError( 'infuse_ads_txt_invalid_subdomain', $subdomain, $number, $fullValue );
				}
			}
		} else {
			// Standard entry line.
			$fields = explode( ',', $line );

			if ( 3 <= count( $fields ) ) {
				$exchange    = trim( $fields[0] );
				$accountType = trim( $fields[2] );

				if ( ! preg_match( self::DOMAIN_REGEX, $exchange ) ) {
					$this->validationError( 'infuse_ads_txt_invalid_exchange', $exchange, $number, $fullValue );
				}

				if ( ! preg_match( '/^(RESELLER|DIRECT)$/i', $accountType ) ) {
					$this->validationError( 'infuse_ads_txt_invalid_account_type', $accountType, $number, $fullValue );
				}

				if ( isset( $fields[3] ) ) {
					$tagId = trim( $fields[3] );

					if ( ! empty( $tagId ) && ! preg_match( '/^[a-f0-9]{16}$/', $tagId ) ) {
						$this->validationError( 'infuse_ads_txt_invalid_tagid', $tagId, $number, $fullValue );
					}
				}
			} else {
				$this->validationError( 'infuse_ads_txt_invalid_record', $line, $number, $fullValue );
			}
		}

		return $value;
	}

	/**
	 * Processes validation error.
	 *
	 * @param string $type
	 * @param string $value
	 * @param int $lineNumber
	 * @param string $fullValue
	 * @return string|void
	 */
	private function validationError( $type, $value, $lineNumber, $fullValue ) {
		$messages = $this->getErrorMessages();

		if ( ! isset( $messages[ $type ] ) ) {
			$message = __( 'Unknown error', 'sheknows-infuse' );
		} else {

			$message = sprintf( esc_html( $messages[ $type ] ), '<code>' . esc_html( $value ) . '</code>' );

			$message = sprintf(
				// translators: Error message output. 1: Line number, 2: Error message
				__( 'Custom Ads.txt entries Line %1$s: %2$s', 'sheknows-infuse' ),
				esc_html( $lineNumber ),
				$message
			);
		}

		$this->coreAdmin->validationError( self::OPTION_EXTRA_LINES, 'adx_txt_validation_error', $message, $fullValue );
	}

	/**
	 * Array of custom error messages.
	 *
	 * @return array
	 */
	private function getErrorMessages() {
		return array(
			'infuse_ads_txt_invalid_variable'     => __( 'Unrecognized variable', 'sheknows-infuse' ),
			'infuse_ads_txt_invalid_record'       => __( 'Invalid record', 'sheknows-infuse' ),
			'infuse_ads_txt_invalid_account_type' => __( 'Third field should be RESELLER or DIRECT', 'sheknows-infuse' ),
			// translators: %s: Subdomain
			'infuse_ads_txt_invalid_subdomain'    => __( '%s does not appear to be a valid subdomain', 'sheknows-infuse' ),
			// translators: %s: Exchange domain
			'infuse_ads_txt_invalid_exchange'     => __( '%s does not appear to be a valid exchange domain', 'sheknows-infuse' ),
			// translators: %s: Alphanumeric TAG ID
			'infuse_ads_txt_invalid_tagid'        => __( '%s does not appear to be a valid TAG ID', 'sheknows-infuse' ),
		);
	}

	/**
	 * Check to see if the user has uploaded an actual ads.txt file, which will
	 * preempt us being able to serve the /ads.txt route.
	 *
	 * @return boolean  True if an ads.txt file is present in the WordPress
	 *                  installation's root directory.
	 */
	protected function checkForPhysicalFile() {
		// We cache the status for the lifetime of this request, because PHP
		// doesn't cache info on non-existent files.  So, if the file doesn't
		// exist, which is what we hope, we could end up making several calls to
		// the underlying filesystem.
		if ( ! isset( $this->adsTxtFileExists ) ) {
			$this->adsTxtFileExists = file_exists( ABSPATH . '/ads.txt' );
		}

		return $this->adsTxtFileExists;
	}

	/**
	 * Permalinks are required for this subsystem to work, so this checks that
	 * they're enabled.
	 *
	 * @return boolean  True if Permalinks are enabled.
	 */
	protected function verifyPermalinksAvailable() {
		global $wp_rewrite;

		if ( ! isset( $this->permalinksEnabled ) ) {
			$this->permalinksEnabled = $wp_rewrite->using_permalinks();
		}

		return $this->permalinksEnabled;
	}

	/**
	 * Check the list of active plugins to see if any plugins are installed and
	 * active that we know may also respond to the /ads.txt URI.
	 *
	 * @return array  List of competing plugins.
	 */
	protected function checkForCompetingPlugins() {
		try {
			$plugins = $this->upstreamCommonConfig->get( 'adstxt_plugins' );

			if ( isset( $plugins ) ) {
				$competing = $this->pluginsHelper->filterActiveBySlugs( $plugins );

				return array_map(
					function ( $plugin ) {
						return $plugin['Name'];
					},
					$competing
				);
			}
		} catch ( \Exception $exception ) {
			$this->errorLogging->record( $exception );
		}

		return array();
	}
}
