<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Contracts\SubsystemMetaBoxInput as MetaBoxInputContract;
use SheKnows\Infuse\Plugin;

abstract class SubsystemMetaBoxInput implements MetaBoxInputContract {

	const KEY = 'default';

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * Input name
	 *
	 * @var string
	 */
	protected $key;

	/**
	 * @param Plugin $plugin
	 * @throws \Exception
	 */
	public function __construct( Plugin $plugin ) {
		$this->plugin = $plugin;

		if ( self::KEY === static::KEY ) {
			throw new \Exception( 'The KEY constant should be defined in ' . get_class( $this ) );
		}

		$this->key = $this->plugin->getSlug() . '_' . static::KEY;
	}

	/**
	 * Returns the value of static::KEY property for provided or current post.
	 *
	 * @param int|bool $post_id
	 *
	 * @return string|bool
	 */
	public function getValue( $post_id = false ) {
		if ( $post_id || is_singular() ) {
			$post_id = $post_id ? $post_id : get_queried_object_id();

			return get_post_meta( $post_id, $this->key, true );
		}

		return false;
	}
}
