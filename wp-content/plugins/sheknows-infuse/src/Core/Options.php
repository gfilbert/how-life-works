<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Plugin;

/**
 * Stores administrative user settings and acts as gatekeeper with database.
 *
 * Class Options
 *
 * @package SheKnows\Infuse\Core
 */
class Options {
	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * Keeps track of whether the non-lazy options have been loaded in.
	 *
	 * @var boolean
	 */
	protected $loaded = false;

	/**
	 * The currently-active non-lazy options for the plugin.
	 *
	 * @var array
	 */
	protected $options;

	/**
	 * The plugin's available options, populated with defaults.  The
	 * boomerang_path and infuse_shared_key options are global options provided by core.
	 * All other options are provided by subsystems calling the register method on this
	 * class.
	 *
	 * @var array
	 */
	protected $defaults = array(
		'boomerang_path'    => '',
		'infuse_shared_key' => '',
	);

	/**
	 * The currently-active lazy loaded options for the plugin.
	 *
	 * @var array
	 */
	protected $lazyLoadedOptions = array();

	/**
	 * Due to the potential size of some options, they are stored separately and
	 * only loaded from the database when actually needed.
	 *
	 * @var array
	 */
	protected $lazyLoadedDefaults = array();

	/**
	 * Constructor.
	 *
	 * @param Plugin $plugin
	 */
	public function __construct( Plugin $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 * Load the non-lazy options from the database.
	 */
	protected function load() {
		if ( ! $this->loaded ) {
			$storedOptions = get_option( $this->getStoredName() );

			if ( ! empty( $storedOptions ) && is_array( $storedOptions ) ) {
				$this->options = array_merge( $this->defaults, $storedOptions );
			} else {
				$this->options = $this->defaults;
			}

			$this->loaded = true;
		}
	}

	/**
	 * Return the name of the option as stored in the database;
	 * @param  string|null  $name  The optional name.  If name is not passed,
	 *                             the plugin slug will be returned, otherwise
	 *                             the name will be returned, prefixed with the
	 *                             plugin's slug.
	 * @return string              The name of the option as passed to
	 *                             get_option, etc.
	 */
	public function getStoredName( $name = null ) {
		$storedName = str_replace( '-', '_', $this->plugin->getSlug() );

		if ( ! empty( $name ) ) {
			$storedName .= "_$name";
		}

		return $storedName;
	}

	/**
	 * Check to see if a given lazy-loaded option has been loaded, and load it
	 * if it hasn't.  Return the value.
	 *
	 * @param  string $name The name of the option.
	 * @return mixed
	 */
	protected function getLazyLoadedOption( $name ) {
		if ( ! array_key_exists( $name, $this->lazyLoadedOptions ) ) {
			$storedName = $this->getStoredName( $name );
			$default    = $this->lazyLoadedDefaults[ $name ];

			$value = get_option( $storedName );

			if ( false === $value ) {
				// If the lazy loaded value hasn't been stored in the database
				// yet, we go ahead and populate it.  If we don't, then saving
				// via the options page may cause this option to be eagerly-
				// loaded on the site.
				// NOTE: In the admin pages, update_option ends up calling the
				// sanitizer methods, which calls back into Options::get, which
				// leads us back here, potentially causing an infinite loop if
				// we don't set the lazyLoadedOptions key before calling
				// update_option().
				$this->lazyLoadedOptions[ $name ] = $default;
				update_option( $storedName, $default, false );
			} else {
				$this->lazyLoadedOptions[ $name ] = $value;
			}
		}

		return $this->lazyLoadedOptions[ $name ];
	}

	/**
	 * Allow a subsystem to register its options with us.
	 *
	 * @param  string  $name        The name of the option.
	 * @param  mixed   $default     The default value for this option.
	 * @param  boolean $lazyLoaded  Optional, set to true if the option should
	 *                              be lazy loaded.
	 */
	public function register( $name, $default, $lazyLoaded = false ) {
		if ( $lazyLoaded ) {
			$this->lazyLoadedDefaults[ $name ] = $default;
		} else {
			$this->defaults[ $name ] = $default;

			if ( $this->loaded && ! isset( $this->options[ $name ] ) ) {
				$this->options[ $name ] = $default;
			}
		}
	}

	/**
	 * Retrieves a specific option value.
	 *
	 * @param  string $name
	 * @return mixed
	 */
	public function get( $name ) {
		if ( array_key_exists( $name, $this->lazyLoadedDefaults ) ) {
			return $this->getLazyLoadedOption( $name );
		} elseif ( array_key_exists( $name, $this->defaults ) ) {
			$this->load();
			return $this->options[ $name ];
		}
	}

	/**
	 * Updates an option.
	 *
	 * @param string $name
	 * @param mixed  $value
	 */
	public function set( $name, $value ) {
		if ( array_key_exists( $name, $this->lazyLoadedDefaults ) ) {
			$this->lazyLoadedOptions[ $name ] = $value;
			// Ensure autoload is false for lazy-loaded options.
			update_option( $this->getStoredName( $name ), $value, false );
		} elseif ( array_key_exists( $name, $this->defaults ) ) {
			$this->load();
			$this->options[ $name ] = $value;
			update_option( $this->getStoredName(), $this->options, true );
		}
	}


	/**
	 * Removes all Infuse-related options from the database.
	 */
	public function clear() {
		delete_option( $this->getStoredName() );

		foreach ( array_keys( $this->lazyLoadedDefaults ) as $name ) {
			delete_option( $this->getStoredName( $name ) );
		}
	}

}
