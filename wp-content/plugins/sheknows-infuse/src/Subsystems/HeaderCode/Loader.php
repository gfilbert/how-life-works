<?php

namespace SheKnows\Infuse\Subsystems\HeaderCode;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Handles writing the Boomerang header code to the page.
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\HeaderCode
 */
class Loader extends SubsystemLoader {

	/**
	 * Initializes binding relevant classes and relevant WordPress API hooks.
	 */
	public function load() {
		$this->bindSubsystemClasses();
		$this->registerOptions();
		$this->registerSubsystemHooks();
	}

	/**
	 * Bind our classes into the container.
	 */
	protected function bindSubsystemClasses() {
		$this->container->bind( __NAMESPACE__ . '\HeaderCodeWriter', array( $this, 'bindWriter' ) );
		$this->container->bind( __NAMESPACE__ . '\Admin', array( $this, 'bindAdmin' ), 'SubsystemAdmin' );
		$this->container->bind( __NAMESPACE__ . '\Tracker', array( $this, 'bindTracker' ), 'SubsystemTracker' );
		$this->container->bind( __NAMESPACE__ . '\DisableAds', array( $this, 'bindDisableAds' ), 'SubsystemMetaBoxInput' );
		$this->container->bind( __NAMESPACE__ . '\Sponsored', array( $this, 'bindSponsored' ), 'SubsystemMetaBoxInput' );
		$this->container->bind( __NAMESPACE__ . '\Vertical', array( $this, 'bindVertical' ), 'SubsystemMetaBoxInput' );
	}

	/**
	 * Register this subsystem's settings.
	 */
	protected function registerOptions() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );

		$options->register( 'header_code_enabled', true );
	}

	/**
	 * Adds hooks to the WordPress API.
	 */
	protected function registerSubsystemHooks() {
		if ( ! is_admin() ) {
			add_action( 'wp_head', array( $this, 'loadWriter' ) );
		}
	}

	/**
	 * If an ad path has been set loads an instance of HeaderCodeWriter
	 */
	public function loadWriter() {
		$options       = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$boomerangPath = $options->get( 'boomerang_path' );
		$enabled       = $options->get( 'header_code_enabled' );

		if ( ! empty( $boomerangPath ) && $enabled ) {
			$this->container->resolve( __NAMESPACE__ . '\HeaderCodeWriter' )->writeHeaderCode( $boomerangPath );
		}
	}

	/**
	 * Returns instance of HeaderCodeWriter class
	 *
	 * @return HeaderCodeWriter
	 */
	public function bindWriter() {
		$viewFactory  = $this->container->resolve( 'SheKnows\Infuse\Core\ViewFactory' );
		$errorLogging = $this->container->resolve( 'SheKnows\Infuse\Core\ErrorLogging' );
		$disableAds   = $this->container->resolve( __NAMESPACE__ . '\DisableAds' );
		$vertical     = $this->container->resolve( __NAMESPACE__ . '\Vertical' );
		$sponsored    = $this->container->resolve( __NAMESPACE__ . '\Sponsored' );

		return new HeaderCodeWriter( $viewFactory, $errorLogging, $disableAds, $vertical, $sponsored );
	}

	/**
	 * Constructs the Admin class for the container.
	 *
	 * @return Admin
	 */
	public function bindAdmin() {
		$options   = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin    = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$coreAdmin = $this->container->resolve( 'SheKnows\Infuse\Core\Admin' );

		return new Admin( $options, $plugin, $coreAdmin );
	}

	/**
	 * Constructs tracker class
	 *
	 * @return Tracker
	 */
	public function bindTracker() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin  = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Tracker( $options, $plugin );
	}

	/**
	 * Constructs DisableAds class.
	 *
	 * @return DisableAds
	 * @throws \Exception
	 */
	public function bindDisableAds() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new DisableAds( $plugin );
	}

	/**
	 * Constructs Vertical class.
	 *
	 * @return Vertical
	 * @throws \Exception
	 */
	public function bindVertical() {
		$plugin               = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$upstreamCommonConfig = $this->container->resolve( 'SheKnows\Infuse\Core\UpstreamCommonConfig' );
		$errorLogging         = $this->container->resolve( 'SheKnows\Infuse\Core\ErrorLogging' );

		return new Vertical( $plugin, $upstreamCommonConfig, $errorLogging );
	}

	/**
	 * Constructs Sponsored class.
	 *
	 * @return Sponsored
	 * @throws \Exception
	 */
	public function bindSponsored() {
		$plugin = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Sponsored( $plugin );
	}
}
