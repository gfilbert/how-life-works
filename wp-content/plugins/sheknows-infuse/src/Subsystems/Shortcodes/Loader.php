<?php

namespace SheKnows\Infuse\Subsystems\Shortcodes;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Handles loading of classes required for shortcode functionality
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\Shortcodes
 */
class Loader extends SubsystemLoader {
	/**
	 * Initializes binding relevant classes and relevant WordPress API hooks.
	 */
	public function load() {
		$this->bindSubsystemClasses();
		$this->registerSubsystemHooks();
	}

	/**
	 * Makes relevant classes available at runtime.
	 */
	protected function bindSubsystemClasses() {
		$this->container->bind( __NAMESPACE__ . '\ShortCodeHandler', array( $this, 'bindShortCodeHandler' ) );
		$this->container->bind( __NAMESPACE__ . '\HelpTab', array( $this, 'bindHelpTab' ) );
	}

	/**
	 * Returns an instance of ShortCodeHandler
	 *
	 * @return ShortCodeHandler
	 */
	public function bindShortCodeHandler() {
		$viewFactory  = $this->container->resolve( 'SheKnows\Infuse\Core\ViewFactory' );
		$options      = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$adTypes      = $this->container->resolve( 'SheKnows\Infuse\Core\AdTypes' );
		$plugin       = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$disableAds   = $this->container->resolve( 'SheKnows\Infuse\Subsystems\HeaderCode\DisableAds' );
		$errorLogging = $this->container->resolve( 'SheKnows\Infuse\Core\ErrorLogging' );

		return new ShortCodeHandler( $viewFactory, $options, $adTypes, $plugin, $disableAds, $errorLogging );
	}

	/**
	 * Constructs the HelpTab class.
	 *
	 * @return HelpTab
	 */
	public function bindHelpTab() {
		$plugin      = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$viewFactory = $this->container->resolve( 'SheKnows\Infuse\Core\ViewFactory' );
		$adTypes     = $this->container->resolve( 'SheKnows\Infuse\Core\AdTypes' );

		return new HelpTab( $plugin, $viewFactory, $adTypes );
	}

	/**
	 * Adds hook to the WordPress API. The sheknows_ad/shemedia_ad shortcodes triggers
	 * rendering of an inline ad.
	 */
	protected function registerSubsystemHooks() {
		add_shortcode( 'sheknows_ad', array( $this, 'initShortcode' ) );
		add_shortcode( 'shemedia_ad', array( $this, 'initShortcode' ) );

		if ( is_admin() ) {
			$hooks = array(
				'post.php',
				'post-new.php',
			);

			foreach ( $hooks as $hook ) {
				add_action( 'load-' . $hook, array( $this, 'addHelp' ) );
			}
		}
	}

	/**
	 * Handler for sheknows_ad/shemedia_ad shortcodes. Instantiates
	 * the shortcode handler. Note that this function is called by WordPress.
	 *
	 * @param array $args
	 * @param string $content Note: this is unused by this shortcode
	 * @param string $tag
	 * @return string
	 */
	public function initShortcode( $args, $content, $tag ) { // phpcs:ignore Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassBeforeLastUsed
		$shortCodeHandler = $this->container->resolve( __NAMESPACE__ . '\ShortCodeHandler' );

		return $shortCodeHandler->handle( $args, $tag );
	}

	/**
	 * Add the help tab to the appropriate pages.
	 */
	public function addHelp() {
		$this->container->resolve( __NAMESPACE__ . '\HelpTab' )->show();
	}
}
