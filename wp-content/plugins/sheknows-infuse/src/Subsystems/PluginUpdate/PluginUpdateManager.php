<?php

namespace SheKnows\Infuse\Subsystems\PluginUpdate;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Core\Options;
use Puc_v4_Factory;

/**
 * Handles interfacing with the plugin-update-checker.
 *
 * Class PluginUpdateManager
 */
class PluginUpdateManager {
	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * PluginUpdateManager constructor.
	 *
	 * @param Plugin   $plugin
	 * @param Options  $options
	 */
	public function __construct( Plugin $plugin, Options $options ) {
		$this->plugin  = $plugin;
		$this->options = $options;
	}

	/**
	 * Initializes update checking and adds an auto update hook.
	 */
	public function checkPluginUpdate() {
		Puc_v4_Factory::buildUpdateChecker(
			'https://infuse.shemedia.com/manifest.json',
			$this->plugin->getFile(),
			$this->plugin->getSlug()
		);

		add_filter( 'auto_update_plugin', array( $this, 'enableAutoUpdatesForPlugins' ), 10, 2 );
	}

	/**
	 * WordPress call this function to check if a plugin has been whitelisted
	 * for automatic updates.
	 *
	 * @param  boolean  $update  Previous update decision.
	 * @param  string   $item    Slug of the plugin being checked.
	 * @return boolean           True if auto updates have been enabled and the
	 *                           plugin being checked is ours.
	 */
	public function enableAutoUpdatesForPlugins( $update, $item ) {
		if ( $this->options->get( 'autoupdate_enabled' ) && $item->slug === $this->plugin->getSlug() ) {
			return true;
		}

		return $update;
	}

}
