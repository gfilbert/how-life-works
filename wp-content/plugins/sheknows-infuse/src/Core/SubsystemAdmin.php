<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Contracts\SubsystemAdmin as AdminContract;

abstract class SubsystemAdmin implements AdminContract {
	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var Admin
	 */
	protected $coreAdmin;

	/**
	 * Constructor.
	 *
	 * @param Options $options
	 * @param Plugin  $plugin
	 * @param Admin   $coreAdmin
	 */
	public function __construct( Options $options, Plugin $plugin, Admin $coreAdmin ) {
		$this->options   = $options;
		$this->plugin    = $plugin;
		$this->coreAdmin = $coreAdmin;
	}

	/**
	 * Called by the Core Admin class to allow subsystems to sanitize any
	 * non-lazy-loaded options that they own.
	 *
	 * @param  array $input  The array of non-lazy-loaded options.
	 * @return array         The sanitized input values from this subsystem.
	 */
	abstract public function sanitize( $input );

	/**
	 * Allow a subsystem to register any lazy loaded settings, sections, and
	 * fields it provides for options pages.
	 */
	abstract public function register();
}
