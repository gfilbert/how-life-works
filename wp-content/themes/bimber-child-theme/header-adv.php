<?php
/**
 * The Header for our theme.
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme 5.4
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_html_class = array(
	'no-js',
);
$bimber_html_class = apply_filters( 'bimber_html_class', $bimber_html_class );

if ( ! in_array( 'g1-off-inside', $bimber_html_class ) ) {
	$bimber_html_class[] = 'g1-off-outside';
}
?><!DOCTYPE html>
<!--[if IE 8]>
<html class="<?php echo implode( ' ', array_map( 'sanitize_html_class', $bimber_html_class ) ); ?> lt-ie10 lt-ie9" id="ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]>
<html class="<?php echo implode( ' ', array_map( 'sanitize_html_class', $bimber_html_class ) ); ?> lt-ie10" id="ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if !IE]><!-->
<html class="<?php echo implode( ' ', array_map( 'sanitize_html_class', $bimber_html_class ) ); ?>" <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<link rel="profile" href="http://gmpg.org/xfn/11"/>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

	<?php wp_head(); ?>
</head>

<body <?php body_class('adv'); ?> <?php bimber_render_microdata( array( 'itemscope' => '', 'itemtype' => 'http://schema.org/WebPage' ) ); ?>>
<?php do_action( 'bimber_body_start' ); ?>

<div class="g1-body-inner">

	<div id="page">
	<div class="adv-header">
		<div class="g1-row-inner">
		<div class="adv-header-left">
			<img src="https://howlifeworks.com/wp-content/uploads/2020/01/hlw_logo_final-amp.png" width="233" height="32" alt="Savetastic">
		</div>
			<div class="adv-header-right">
				<nav class="g1-primary-nav"><ul class="g1-primary-nav-menu g1-menu-h">
				<li class="button menu-item menu-item-type-custom menu-item-object-custom menu-item-g1-standard menu-item-1740"><a target="_blank" rel="noopener" href="https://chrome.google.com/webstore/detail/savetastic-cashback-coupo/agcaedeoaoeknlmoeiladhcelgnfpfnh?hl=en-US">Add to Chrome — It’s Free</a></li></ul></nav>
			</div>
			</div>
		</div>
