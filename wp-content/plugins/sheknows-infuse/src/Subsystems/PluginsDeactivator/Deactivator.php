<?php

namespace SheKnows\Infuse\Subsystems\PluginsDeactivator;

use SheKnows\Infuse\Core\ErrorLogging;
use SheKnows\Infuse\Core\ViewFactory;
use SheKnows\Infuse\Core\UpstreamSiteConfig;
use SheKnows\Infuse\Core\PluginsHelper;
use SheKnows\Infuse\Core\UpstreamCommonConfig;
use SheKnows\Infuse\Plugin;

/**
 * Provides the functionality to detect/notify/display/disable competing plugins.
 *
 * Class Deactivator
 *
 * @package SheKnows\Infuse\Subsystems\PluginsDeactivator
 */
class Deactivator {

	/**
	 * Name of nonce to verify requests to deactivate plugins.
	 */
	const NONCE_NAME = 'sheknows_infuse_competing_plugins_deactivation';

	/**
	 * @var array Array of active competing plugins.
	 */
	private $plugins;

	/**
	 * Refresh URL for competing plugins list page.
	 *
	 * @var string
	 */
	protected $refreshUrl = '';

	/**
	 * @var ViewFactory
	 */
	private $viewFactory;

	/**
	 * @var UpstreamCommonConfig
	 */
	private $commonConfig;

	/**
	 * @var UpstreamSiteConfig
	 */
	private $siteConfig;

	/**
	 * @var ErrorLogging
	 */
	private $errorLogging;

	/**
	 * @var PluginsHelper
	 */
	private $pluginsHelper;

	/**
	 * @var Plugin
	 */
	private $plugin;

	/**
	 * @var string
	 */
	private $slug;

	/**
	 * @param ViewFactory $viewFactory
	 * @param UpstreamCommonConfig $upstreamCommonConfig
	 * @param UpstreamSiteConfig $siteConfig
	 * @param ErrorLogging $errorLogging
	 * @param PluginsHelper $pluginsHelper
	 * @param Plugin $plugin
	 * @param string $slug
	 */
	public function __construct( ViewFactory $viewFactory, UpstreamCommonConfig $upstreamCommonConfig, UpstreamSiteConfig $siteConfig, ErrorLogging $errorLogging, PluginsHelper $pluginsHelper, Plugin $plugin, $slug ) {
		$this->viewFactory   = $viewFactory;
		$this->commonConfig  = $upstreamCommonConfig;
		$this->siteConfig    = $siteConfig;
		$this->errorLogging  = $errorLogging;
		$this->pluginsHelper = $pluginsHelper;
		$this->plugin        = $plugin;
		$this->slug          = $slug;
	}

	/**
	 * Checks if we have active competing plugins.
	 *
	 * @return bool
	 */
	public function checkPlugins() {
		try {
			$exclusiveContract = $this->siteConfig->get( 'exclusive_contract' );
		} catch ( \Exception $exception ) {
			$this->errorLogging->record( $exception );
			return false;
		}

		$plugins = $this->getPlugins();

		if ( $exclusiveContract && ! empty( $plugins ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Retrieves list of competing plugins and checks if they're active.
	 *
	 * @param boolean $reset
	 * @return array
	 */
	public function getPlugins( $reset = false ) {
		if ( is_array( $this->plugins ) && ! $reset ) {
			return $this->plugins;
		}

		$this->plugins = array();

		try {
			$plugins       = $this->commonConfig->get( 'competing_plugins' );
			$this->plugins = $this->pluginsHelper->filterActiveBySlugs( $plugins );
		} catch ( \Exception $exception ) {
			$this->errorLogging->record( $exception );
		}

		return $this->plugins;
	}

	/**
	 * Renders the page with list of active competing plugins.
	 */
	public function displayPluginsList() {
		$this->processDeactivation();

		$vars = array(
			'plugins'     => $this->getPlugins(),
			'nonce'       => wp_create_nonce( self::NONCE_NAME ),
			'refresh_url' => $this->refreshUrl,
			'logo'        => plugin_dir_url( $this->plugin->getFile() ) . 'public/images/logo.png',
			'page_slug'   => $this->slug,
		);

		// phpcs:ignore WordPress.Security.EscapeOutput
		echo $this->viewFactory->make( 'admin/competingplugins', $vars )->render();
	}

	/**
	 * Deactivates plugins by provided slug|array of slugs.
	 *
	 * @param array|string $plugins
	 */
	private function deactivate( $plugins ) {
		$plugins = is_array( $plugins ) ? $plugins : array( $plugins );
		deactivate_plugins( $plugins );
	}

	/**
	 * Checks if user wants to deactivate plugins and deactivate them.
	 */
	private function processDeactivation() {
		if ( ! isset( $_GET['action'] ) || ! isset( $_GET['slug'] ) ) {
			return null;
		}

		if ( ! wp_verify_nonce( $_GET['nonce'], self::NONCE_NAME ) ) {
			return null;
		}

		if ( 'deactivate' !== $_GET['action'] ) {
			return null;
		}

		$slug = $_GET['slug'];

		$competingPlugins = $this->getPlugins();

		if ( 'all' === $slug ) {
			$plugins = array_keys( $competingPlugins );
		} else {
			$plugins = in_array( $slug, array_keys( $competingPlugins ), true ) ? array( $slug ) : array();
		}

		if ( is_array( $plugins ) && ! empty( $plugins ) ) {
			$this->deactivate( $plugins );

			$leftoverPlugins = $this->getPlugins( true );

			if ( empty( $leftoverPlugins ) ) {
				$this->refreshUrl = $this->plugin->getOptionsPageUrl();
			}
		}
	}
}
