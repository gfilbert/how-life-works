<?php

namespace SheKnows\Infuse\Subsystems\AdWidget;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Core\ViewFactory;

/**
 * Adds help tab content where applicable.
 *
 * Class HelpTab
 *
 * @package SheKnows\Infuse\Subsystems\AdWidget
 */
class HelpTab {
	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var ViewFactory
	 */
	protected $viewFactory;

	/**
	 * HelpTab constructor.
	 *
	 * @param Plugin       $plugin
	 * @param ViewFactory  $viewFactory
	 */
	public function __construct( Plugin $plugin, ViewFactory $viewFactory ) {
		$this->plugin      = $plugin;
		$this->viewFactory = $viewFactory;
	}

	/**
	 * Show the help tab on the appropriate screens.
	 */
	public function show() {
		$screen = get_current_screen();

		if ( $screen ) {
			$screen->add_help_tab(
				array(
					'id'       => $this->plugin->getSlug() . '-ad-widgets',
					'title'    => 'SHE Media Ad Widget',
					'content'  => $this->viewFactory->make( 'helptabs/adwidgets' )->render(),
					'priority' => 20,
				)
			);
		}
	}
}
