<?php

namespace SheKnows\Infuse\Subsystems\AdWidget;

use WP_Widget;

/**
 * Main wordpress-facing widget class
 *
 * Class _Widget
 */
class Widget extends WP_Widget {

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * Because this class is actually instantiated directly by WordPress, not by
	 * our container, we can't bind its dependencies to it.  We therefore attach
	 * the container to the subsystem's Loader class statically and access it
	 * here using the Loader class' static method.  WordPress version 4.6 fixes
	 * this issue by allowing plugins to provide an instantiated WP_Widget
	 * object to the register_widget() function rather than instantiating the
	 * object itself from the provided class name string.  Since we support 3.7
	 * and up, we can't rely on this feature yet.
	 *
	 * Widget constructor.
	 */
	public function __construct() {
		$this->container = Loader::getContainer();
		$this->plugin    = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$this->options   = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$slug            = $this->plugin->getSlug() . '-ad-widget';

		parent::__construct(
			$slug,
			__( 'SHE Media Ad', 'sheknows-infuse' ),
			array(
				'classname'   => $slug,
				'description' => __( 'Displays a SHE Media ad.', 'sheknows-infuse' ),
			)
		);
	}

	/**
	 * Public portion of the widget. Renders an ad.
	 *
	 * @param array  $args
	 * @param array  $instance
	 */
	public function widget( $args, $instance ) {
		$enabled       = $this->options->get( 'header_code_enabled' );
		$boomerangPath = $this->options->get( 'boomerang_path' );

		if ( ! $enabled || empty( $boomerangPath ) ) {
			return '';
		}

		// phpcs:ignore WordPress.Security.EscapeOutput
		echo $this->container->resolve( __NAMESPACE__ . '\WidgetPublic' )->render( $args, $instance );
	}

	/**
	 * Handles form submissions from widget admin.
	 *
	 * @param  array  $new_instance
	 * @param  array  $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) { // phpcs:ignore Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassAfterLastUsed
		$adminForm = $this->container->resolve( __NAMESPACE__ . '\WidgetAdmin' );

		return $adminForm->update( $new_instance );
	}

	/**
	 * Displays widget admin form
	 *
	 * @param array $instance
	 */
	public function form( $instance ) {
		$adminForm = $this->container->resolve( __NAMESPACE__ . '\WidgetAdmin' );

		// phpcs:ignore WordPress.Security.EscapeOutput
		echo $adminForm->renderForm( $this, $instance );
	}
}
