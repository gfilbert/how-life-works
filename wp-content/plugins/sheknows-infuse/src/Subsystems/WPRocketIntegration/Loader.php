<?php

namespace SheKnows\Infuse\Subsystems\WPRocketIntegration;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Handles integration with WP Rocket.
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\WPRocketIntegration
 */
class Loader extends SubsystemLoader {

	/**
	 * Initializes binding relevant classes and relevant WordPress API hooks.
	 */
	public function load() {
		$this->registerSubsystemHooks();
	}

	/**
	 * Adds hook to the WordPress API.
	 */
	protected function registerSubsystemHooks() {
		add_filter( 'rocket_minify_excluded_external_js', array( $this, 'excludeSheMediaJSDomains' ) );
		add_filter( 'rocket_excluded_inline_js_content', array( $this, 'excludeSheMediaInlineJS' ) );
	}

	/**
	 * Add SHE Media domains to WP Rocket's external JavaScript exclusion list.
	 *
	 * @param array $excludedJS
	 * @return array $excludedJS
	 */
	public function excludeSheMediaJSDomains( $excludedJS ) {
		$excludedJS[] = 'ads.blogherads.com';
		$excludedJS[] = 'ads.shemedia.com';

		return $excludedJS;
	}

	/**
	 * Add SHE Media inline JS to WP Rocket's inline JavaScript exclusion list.
	 *
	 * @param array $excludedContent
	 * @return array $excludedContent
	 */
	public function excludeSheMediaInlineJS( $excludedContent ) {
		$excludedContent[] = 'blogherads';

		return $excludedContent;
	}
}
