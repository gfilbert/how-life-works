<?php

namespace SheKnows\Infuse\Core;

/**
 * Retrieves and Caches error logs.
 *
 * @package SheKnows\Infuse\Core
 */
class ErrorLogging {

	/**
	 * Transient name for Infuse error logs.
	 */
	const TRANSIENT_NAME = 'infuse_error_logs';

	/**
	 * Expiration time.
	 */
	const EXPIRE = 604800;

	/**
	 * Size of error log.
	 */
	const SIZE = 20;

	/**
	 * Retrieves list of error logs from cache.
	 *
	 * @return mixed
	 */
	public function get() {
		return get_transient( static::TRANSIENT_NAME );
	}

	/**
	 * Prepends error log to the cached list.
	 *
	 * @param \Exception $error
	 */
	public function record( $error ) {
		$cache = $this->get();
		$cache = $cache ? $cache : array();

		$data = array(
			'timestamp' => gmdate( DATE_ISO8601 ),
			'data'      => array(
				'message' => $error->getMessage(),
				'code'    => $error->getCode(),
				'file'    => $error->getFile(),
				'line'    => $error->getLine(),
				'trace'   => $error->getTraceAsString(),
			),
		);

		array_unshift( $cache, $data );

		set_transient( static::TRANSIENT_NAME, array_slice( $cache, 0, static::SIZE ), static::EXPIRE );
	}

	/**
	 * Removes all error logs from cache.
	 */
	public function clear() {
		delete_transient( static::TRANSIENT_NAME );
	}
}
