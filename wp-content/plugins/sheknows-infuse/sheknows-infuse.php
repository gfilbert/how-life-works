<?php
/**
 * The main plugin file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://infuse.shemedia.com/
 * @since             1.0.0
 * @package           SHE_Media_Infuse
 *
 * @wordpress-plugin
 * Plugin Name:       SHE Media Infuse
 * Plugin URI:        https://www.shemedia.com/installing-and-configuring-sheknows-infuse/
 * Description:       SHE Media Infuse simplifies running SHE Media advertising and related technologies on your site.
 * Version:           1.0.29
 * Author:            SHE Media
 * Author URI:        http://www.shemedia.com
 * Text Domain:       sheknows-infuse
 * Domain Path:       /languages
 */

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'SHEKNOWS_INFUSE_VERSION', '1.0.29' );
define( 'SHEKNOWS_INFUSE_PATH', __FILE__ );


// Simple protection to ensure that the plugin doesn't crater/whitescreen sites
// that are running unsupported versions of PHP.
function sheknows_infuse_php_version_warning() {
	$class = 'notice notice-warning is-dismissible';

	// translators: PHP minimum version requirement error message.
	$message = __( 'SHE Media Infuse requires PHP version 5.3 or higher.  This site currently utilizes PHP version %1$s.  Please contact your hosting provider to request they upgrade your version of PHP to continue using SHE Media Infuse or deactivate the SHE Media Infuse plugin to disable this warning.', 'sheknows-infuse' );
	$message = sprintf( $message, phpversion() );

	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
}

if ( version_compare( PHP_VERSION, '5.3.0' ) < 0 ) {
	add_action( 'admin_notices', 'sheknows_infuse_php_version_warning' );
} else {
	include dirname( __FILE__ ) . '/start.php';
}
