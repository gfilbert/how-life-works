<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// phpcs:disable WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
// phpcs:disable WordPress.WP.GlobalVariablesOverride.Prohibited
?>

<div class="wrap">
	<img src="<?php echo esc_attr( $logo ); ?>" class="sheknows-logo"/>
	<h1>SHE Media Infuse Settings</h1>
	<!-- For compatibility with pre-WordPress-4.3 installations, otherwise error messages don't display in the proper location -->
	<h2 style="display: none;"></h2>
	<form method="post" action="options.php">
		<?php settings_fields( $settings_fields ); ?>
		<?php do_settings_sections( $slug ); ?>
		<?php submit_button(); ?>
	</form>
	<p><span class="red-text">*</span> Required field.</p>

	<?php if ( ! empty( $errors ) || ! empty( $boomerang_path ) ) : ?>
		<div>
			<hr/>
			<h2 class="infuse-troubleshooting-header">
				<span class="dashicons dashicons-arrow-up"></span>Troubleshooting
			</h2>
			<div class="infuse-troubleshooting-container">
				<?php if ( ! empty( $boomerang_path ) ) : ?>
					<h3>Reconfigure Infuse</h3>
					<p>
						<a href="<?php echo esc_url( $autoconfig_link ); ?>">Click here</a>
						to reconfigure Infuse. You should not need to click this link unless
						Infuse has stopped working or Support has requested that you do so.
					</p>
				<?php endif; ?>
				<?php if ( ! empty( $errors ) ) : ?>
					<h3>Infuse Errors</h3>
					<textarea readonly rows="5" class="infuse-errors-textarea">
						<?php
						foreach ( $errors as $error ) {
							echo "\n" . esc_textarea( $error['timestamp'] ) . "\n";

							echo esc_textarea( $error['data']['message'] ) . ' (' . esc_textarea( $error['data']['code'] ) . ")\n";
							echo esc_textarea( $error['data']['file'] ) . ' (' . esc_textarea( $error['data']['line'] ) . ")\n";
							echo esc_textarea( $error['data']['trace'] ) . "\n";

							echo "==================================================\n";
						}
						?>
					</textarea>
				<?php endif; ?>
			</div>
			<hr/>
		</div>
	<?php endif; ?>
</div>
