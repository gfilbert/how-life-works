<?php 
defined( 'ABSPATH' ) || exit;
return array (
  'homepage' => 'http://localhost:8888/howlifeworks.com',
  'cache_options' => 
  array (
    'breeze-active' => '1',
    'breeze-ttl' => '',
    'breeze-minify-html' => '0',
    'breeze-minify-css' => '0',
    'breeze-minify-js' => '0',
    'breeze-gzip-compression' => '1',
    'breeze-desktop-cache' => '1',
    'breeze-browser-cache' => '1',
    'breeze-mobile-cache' => '1',
    'breeze-disable-admin' => '1',
    'breeze-display-clean' => '1',
    'breeze-include-inline-js' => '0',
    'breeze-include-inline-css' => '0',
  ),
  'disable_per_adminuser' => '1',
  'exclude_url' => 
  array (
  ),
  'enabled-lazy-load' => 0,
  'use-lazy-load-native' => 0,
  'breeze-preload-links' => 0,
  'woocommerce_geolocation_ajax' => 0,
  'permalink_structure' => '/%category%/%postname%/',
); 
