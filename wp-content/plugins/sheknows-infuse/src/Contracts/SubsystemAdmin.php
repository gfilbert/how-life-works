<?php

namespace SheKnows\Infuse\Contracts;

use SheKnows\Infuse\Core\Admin;

interface SubsystemAdmin {
	/**
	 * Called by the Core Admin class to allow subsystems to sanitize any
	 * non-lazy-loaded options that they own.
	 *
	 * @param  array $input  The array of non-lazy-loaded options.
	 * @return array         The sanitized input values from this subsystem.
	 */
	public function sanitize( $input );

	/**
	 * Allow a subsystem to register any lazy loaded settings, sections, and
	 * fields it provides for options pages.
	 */
	public function register();
}
